var TIAA_ud = TIAA_ud || {};

TIAA_ud = {
	udThemeUrl: '/themes/ud_atom/release_2014-12/',
	globalThemeUrl: '/assets/release_2014-12/',
	globalAssetUrl: '/assets/',
	localUrl: 'assets/',
	leftNavMode: '.leftNavMenu', // - For Participant Summary like pages ps' || DEFAULT is 'none' - No Left nav || '$Element' - The section that needs to be on right side,
	oneColWidth: 1080,
	twoColWidth: 980,
	versioning: true,
	// phpMode: true,   			// Use PHP
	phpMode: false,   				// Use jQuery
	scenarioCheck: 'release', 		// For version specific scenario - enter release value
	panelToggleVisible: true, 		// Panel toggling
	startVersion: 'jul2016',  		// Starting version
	cssName: 'ud_tele_enroll.css',	// Proj Specific CSS	
	jsName: 'ud_tele_enroll.js',	// Proj Specific JS
	projLoc: '/ud/tele_enroll/'		// Location of currect project from prototype root
};



// This is collective object data for Unified desktop
	TIAA_ud.releaseObj = {
		// DO NOT EDIT THE BELOW

		// All these are current default codes
		defaults: {
			proCSS: true,
			left: true,
			header: true,  
			pagetitle: true,
			body: true,
			popups: true,
			proJS: true
		},
		// DO NOT EDIT THE ABOVE CODE


		// FEEL FREE TO UPDATE THE BELOW CODE
		// demo: {
		// 	tele_enroll: {
		// 		scenario: true,
		// 		left: true,
		// 		body: true,
		// 		proCSS: true,
		// 		proJS: true,
		// 		popups: true
		// 	}
		// },
		// 'demo-sept': {
		// 	tele_enroll: {
		// 		scenario: true,
		// 		left: true,
		// 		body: true,
		// 		proCSS: true,
		// 		proJS: true,
		// 		popups: true
		// 	}
		// },
		// s4: {
		// 	tele_enroll: {
		// 		left: true,
		// 		body: true,
		// 		pagetitle: true,
		// 		scenario: true
		// 	}
		// },
		// s6: {
		// 	tele_enroll: {
		// 		left: true,
		// 		body: true,
		// 		scenario: true
		// 	}
		// },
		// s7: {
		// 	tele_enroll: {
		// 		left: true,
		// 		proCSS: true,
		// 		body: true,
		// 		proJS: true,
		// 		scenario: true
		// 	}
		// },
		// s8: {
		// 	tele_enroll: {
		// 		scenario: true,
		// 		left: true,
		// 		body: true,
		// 		proJS: true
		// 	}
		// },
		// s10: {
		// 	tele_enroll: {
		// 		scenario: true,
		// 		left: true,
		// 		body: true,
		// 		proCSS: true,
		// 		proJS: true,
		// 		popups: true
		// 	}
		// },		
		// s11: {
		// 	tele_enroll: {
		// 		scenario: true,
		// 		left: true,
		// 		body: true,
		// 		proCSS: true,
		// 		proJS: true,
		// 		popups: true
		// 	}
		// },
		
		// s13: {
		// 	tele_enroll: {
		// 		scenario: true,
		// 		left: true,
		// 		body: true,
		// 		proCSS: true,
		// 		proJS: true,
		// 		popups: true
		// 	}
			
		// },
		// 	s16: {
		// 	tele_enroll: {
		// 		scenario: true,
		// 		left: true,
		// 		body: true,
		// 		proCSS: true,
		// 		proJS: true,
		// 		popups: true
		// 	}
		// },
		//    'demo-mar': {
		// 	tele_enroll: {
		// 		scenario: true,
		// 		left: true,
		// 		body: true,
		// 		proCSS: true,
		// 		proJS: true,
		// 		popups: true
		// 	}
		// },
		// 	s18: {
			
		// 	tele_enroll: {
		// 		scenario: true,
		// 		left: true,
		// 		body: true,
		// 		proCSS: true,
		// 		proJS: true,
		// 		popups: true
		// 	},
		// 	plan1tele_enroll: {
		// 	        scenario: true,
		// 			left:true,
		// 			pagetitle:true,
		// 			body: true,
		// 			proCSS: true,
		// 		    proJS: true
					
		// 		},
		// 	plan2tele_enroll: {
		// 	        scenario: true,
		// 			left:true,
		// 			body: true,
		// 			proCSS: true,
		// 		    proJS: true
					
		// 		},
		// 		planstele_enroll: {
		// 	        scenario: true,
		// 			left:true,
		// 			pagetitle:true,
		// 			body: true,
		// 			proCSS: true,
		// 		    proJS: true
		// 		}
				
		// },
		// 	sprintH: {
			
		// 		tele_enroll: {
		// 			scenario: true,
		// 			left: true,
		// 			body: true,
		// 			proCSS: true,
		// 			proJS: true,
		// 			popups: true
		// 		},
		// 		plan1tele_enroll: {
		// 				scenario: true,
		// 				left:true,
		// 				pagetitle:true,
		// 				body: true,
		// 				proCSS: true,
		// 				proJS: true
		// 			}
		// },
		// 	sprintI: {
			
		// 		tele_enroll: {
		// 			scenario: true,
		// 			left: true,
		// 			body: true,
		// 			proCSS: true,
		// 			proJS: true,
		// 			popups: true
		// 		},
		// 		plan1tele_enroll: {
		// 				scenario: true,
		// 				left:true,
		// 				pagetitle:true,
		// 				body: true,
		// 				proCSS: true,
		// 				proJS: true
		// 			}
		// },
		// 	sprintJ: {
			
		// 		tele_enroll: {
		// 			scenario: true,
		// 			left: true,
		// 			pagetitle:true,
		// 			body: true,
		// 			proCSS: true,
		// 			proJS: true,
		// 			popups: true
		// 		},
		// 		plan1tele_enroll: {
		// 				scenario: true,
		// 				left:true,
		// 				pagetitle:true,
		// 				body: true,
		// 				proCSS: true,
		// 				proJS: true
		// 			}
		// },
			'demo-july': {
				tele_enroll: {
					scenario: true,
					left: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true,
					popups: true
				},
				plan1tele_enroll: {
						scenario: true,
						left:true,
						pagetitle:true,
						body: true,
						proCSS: true,
						proJS: true
					}
		},
			sprintK: {
			
				tele_enroll: {
					scenario: true,
					left: true,
					body: true,
					proCSS: true,
					proJS: true,
					popups: true
				},
				plan1tele_enroll: {
						scenario: true,
						left:true,
						pagetitle:true,
						body: true,
						proCSS: true,
						proJS: true
					}
		},
			'sprintV': {
			
				tele_enroll: {
					scenario: true,
					left: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true,
					popups: true
				},
				plan1tele_enroll: {
						left:true,
						pagetitle:true,
						body: true,
						proCSS: true,
						proJS: true
					}
		},
			'sprintX': {
			
				tele_enroll: {
					scenario: true,
					left: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true,
					popups: true
				},
				plan1tele_enroll: {
						left:true,
						pagetitle:true,
						body: true,
						proCSS: true,
						proJS: true
					}
		},
			'feb2015': {
				themeUrl:{
					udThemeUrl: '/themes/ud2_atom/release_2015-02/',
					globalThemeUrl: '/assets/release_2015-02/'
				},
				tele_enroll: {
					scenario: true,
					left: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true,
					popups: true
				},
				plan1tele_enroll: {
						left:true,
						pagetitle:true,
						body: true,
						proCSS: true,
						proJS: true
					}
		},
		'mar2015': {
				themeUrl:{
					udThemeUrl: '/themes/ud2_atom/release_2015-03/',
					globalThemeUrl: '/assets/release_2015-03/'
				},
				tele_enroll: {
					scenario: true,
					left: true,
					header: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true,
					popups: true
				},
				plan1tele_enroll: {
						left:true,
						header: true,
						pagetitle:true,
						body: true,
						proCSS: true,
						proJS: true
					}
		},
		'jul2015': {
				themeUrl:{
					udThemeUrl: '/themes/ud2_atom/release_2015-06/'
				},
				tele_enroll: {
					scenario: true,
					left: true,
					header: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true,
					popups: true
				},
				plan1tele_enroll: {
					left:true,
					header: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true
				},
				atom_beam1: {
					pagetitle: 'page',
					proCSS: true,
					proJS: true,
					body: 'page'
				},
				atom_beam2: {
					pagetitle: 'page',
					proCSS: true,
					proJS: true,
					body: 'page'
				}
		},
		'aug2015': {
				themeUrl:{
					udThemeUrl: '/themes/ud2_atom/release_2015-06/'
				},
				tele_enroll: {
					scenario: true,
					left: true,
					header: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true,
					popups: true
				},
				plan1tele_enroll: {
					left:true,
					header: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true
				},
				atom_beam1: {
					pagetitle: 'page',
					proCSS: true,
					proJS: true,
					body: 'page'
				},
				atom_beam2: {
					pagetitle: 'page',
					proCSS: true,
					proJS: true,
					body: 'page'
				}
		},
		'sep2015': {
				themeUrl:{
					udThemeUrl: '/themes/ud2_atom/release_2015-06/'
				},
				tele_enroll: {
					scenario: true,
					left: true,
					header: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true,
					popups: true
				},
				plan1tele_enroll: {
					left:true,
					header: true,
					pagetitle:true,
					body: true,
					proCSS: true,
					proJS: true
				},
				atom_beam1: {
					pagetitle: 'page',
					proCSS: true,
					proJS: true,
					body: 'page'
				},
				atom_beam2: {
					pagetitle: 'page',
					proCSS: true,
					proJS: true,
					body: 'page'
				},
				view_details: {
				left:true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true
			}
		},		
		'nov2015': {
			themeUrl:{
				udThemeUrl: '/themes/ud2_atom/release_2015-09/'
			},
			tele_enroll: {
				scenario: true,
				left: true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true,
				popups: true
			},
			plan1tele_enroll: {
				left:true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true
			},
			atom_beam1: {
				pagetitle: 'page',
				proCSS: true,
				proJS: true,
				body: 'page'
			},
			atom_beam2: {
				pagetitle: 'page',
				proCSS: true,
				proJS: true,
				body: 'page'
			},
			view_details: {
				left:true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true
			}
		},		
		'test2015': {
			themeUrl:{
				udThemeUrl: '/themes/ud2_atom/release_2015-09/'
			},
			tele_enroll: {
				scenario: true,
				left: true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true,
				popups: true
			},
			plan1tele_enroll: {
				left:true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true
			},
			atom_beam1: {
				pagetitle: 'page',
				proCSS: true,
				proJS: true,
				body: 'page'
			},
			atom_beam2: {
				pagetitle: 'page',
				proCSS: true,
				proJS: true,
				body: 'page'
			},
			view_details: {
				left:true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true
			}
		},		
		'mar2016': {
			themeUrl:{
				udThemeUrl: '/themes/ud2_atom/release_2015-09/'
			},
			tele_enroll: {
				scenario: true,
				left: true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true,
				popups: true
			},
			plan1tele_enroll: {
				left:true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true
			},
			atom_beam1: {
				pagetitle: 'page',
				proCSS: true,
				proJS: true,
				body: 'page'
			},
			atom_beam2: {
				pagetitle: 'page',
				proCSS: true,
				proJS: true,
				body: 'page'
			},
			view_details: {
				left:true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true
			}
		},		
		'jul2016': {
			themeUrl:{
				udThemeUrl: '/themes/ud2_atom/release_2016-03/'
			},
			tele_enroll: {
				scenario: true,
				left: true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true,
				popups: true
			},
			plan1tele_enroll: {
				left:true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true
			},
			atom_beam1: {
				pagetitle: 'page',
				proCSS: true,
				proJS: true,
				body: 'page'
			},
			atom_beam2: {
				pagetitle: 'page',
				proCSS: true,
				proJS: true,
				body: 'page'
			},
			view_details: {
				left:true,
				header: true,
				pagetitle:true,
				body: true,
				proCSS: true,
				proJS: true
			}
		}		
		
		// FEEL FREE TO UPDATE THE ABOVE CODE
	};

// Version based scenario 
 (function(cont) {
     function checkstore() {
         if (typeof store === 'undefined') {
             setTimeout(function() {
                 checkstore();
             }, 5);
         } else {
         	$(document).trigger('storeLoaded');

         	cont.release = store.get('release');

            cont.sp7Scn = store.get('sp7-scenario');
            if(TIAA_ud.startVersion === 's7' && cont.release === undefined){
				if (cont.sp7Scn === '' || cont.sp7Scn === undefined) {
				    store.set('sp7-scenario', 'noplan');
				    cont.sp7Scn = store.get('sp7-scenario');
				    store.set('user', 'exist');
				};
			};

			cont.sp8Scn = store.get('sp8-scenario');
			if(TIAA_ud.startVersion === 's8' && cont.release === undefined){
				if (cont.sp8Scn === '' || cont.sp8Scn === undefined) {
				    store.set('sp8-scenario', 'ssnNo');
				    store.set('user', 'new');
				    cont.sp8Scn = store.get('sp8-scenario');
				};
			};

			 cont.sp10Scn = store.get('sp10-scenario');
			if(TIAA_ud.startVersion === 's10' && cont.release === undefined){
				store.clear();
				if (cont.sp8Scn === '' || cont.sp8Scn === undefined) {
				    store.set('sp8-scenario', 'ssnYes');
				    store.set('user', 'exist');
				    cont.sp8Scn = store.get('sp8-scenario');
				};
			};
			
			cont.sp11Scn = store.get('sp11-scenario');
			if(TIAA_ud.startVersion === 's11' && cont.release === undefined){
				store.clear();
				if (cont.sp8Scn === '' || cont.sp8Scn === undefined) {
				    store.set('sp8-scenario', 'ssnNo');
				    store.set('user', 'exist');
				    cont.sp8Scn = store.get('sp8-scenario');
				};
			};
			
			cont.userInfo = store.get('user');
			if (cont.userInfo === '' || cont.userInfo === undefined) {
			    store.set('user', 'exist');
			    cont.userInfo = store.get('user');
			};

			if(cont.release === "demo"){ 
				store.clear();
				store.set('release','demo');
				store.set('sp8-scenario', 'ssnNo');
				cont.sp8Scn = store.get('sp8-scenario');
			}

			if(cont.release === "demo-sept"){ 
				store.clear();
				store.set('release','demo-sept');
				store.set('sp8-scenario', 'ssnNo');
				cont.sp8Scn = store.get('sp8-scenario');
			}

			if(cont.release === "s13"){ 
				cont.sp8Scn = store.get('sp8-scenario');
				store.clear();
				store.set('release','s13');
				//console.log(cont.sp8Scn === undefined);
				if (cont.sp8Scn === '' || cont.sp8Scn === undefined || cont.sp8Scn=="ssnYes") {
					store.set('sp8-scenario', 'ssnYes');
					cont.sp8Scn = store.get('sp8-scenario');
				}
				else{
					store.set('sp8-scenario', 'ssnNo');
				}
			}

			cont.sp18Scn = store.get('sp18-scenario');
            if(TIAA_ud.startVersion === 's18' && cont.release === undefined){
            	store.clear();
				if (cont.sp18Scn === '' || cont.sp18Scn === undefined) {
				    store.set('sp18-scenario', 'fastLane');
				    cont.sp18Scn = store.get('sp18-scenario');
				}
			};

			// cont.sp18Scn = store.get('sp18-scenario');
            if(TIAA_ud.startVersion === 'sprintJ' && cont.sp18Scn === undefined){ 
            	store.clear();
				if (cont.sp18Scn === '' || cont.sp18Scn === undefined) {
				    store.set('sp18-scenario', 'fastLane');
				    store.set('sp8-scenario', 'ssnYes');
				    store.set('users', 'exist');
				    cont.sp18Scn = store.get('sp18-scenario');
				    cont.sp8Scn = store.get('sp8-scenario');
				    cont.spusers = store.get('users');
				}
			};

			if(TIAA_ud.startVersion === 'demo-july' && cont.release === undefined){
				if(store.get('users')=== undefined || store.get('address_modal')=== undefined || store.get('address_error')=== undefined){
					store.set('users','noUser');
					store.set('address_modal','true');
					store.set('address_error','true');
				};
			};

			// Updated for Sprint V
			if((TIAA_ud.startVersion === 'sprintV' && cont.release === 'sprintV') || (cont.release === 'sprintV')){
				// if(store.get('users')=== undefined || store.get('address_modal')=== undefined || store.get('address_error')=== undefined){
				// if(store.get('users')=== undefined || store.get('address_modal')=== undefined || store.get('address_error')=== undefined){
					// store.set('users','noUser');
					store.set('address_modal','true');
					store.set('address_descrepency','true');
				// };
			};
			
			// Updated for Sprint X
			if((TIAA_ud.startVersion === 'sprintX' && cont.release === 'sprintX') || (cont.release === 'sprintX')){
				store.set('users','existUser');
			};

			// Updated for Jul2015
			if( (TIAA_ud.startVersion === 'jul2015' || cont.release === 'jul2015' ) &&  store.get('users') === undefined  ){
				store.set('users','existUser');
			};
			
			// Updated for Aug2015
			if( (TIAA_ud.startVersion === 'aug2015' || cont.release === 'aug2015' )  ){
				store.set('users','existUser');
			};

			// Updated for nov2015
			if( (TIAA_ud.startVersion === 'nov2015' || cont.release === 'nov2015' ) &&  store.get('users') === undefined  ){
				store.set('users','existUser');
				store.set('details','srkDetails');
				store.set('suny_rpe','rpeYes');
			};

         };         

     };
        checkstore();
 })(window);

 // Updated the protoSettings
document.write('<script src="/themes/ud_atom/templates/proto_setting.js"></script>');


// // Loading CSS
// document.write('<link type="text/css" rel="stylesheet" href="' + TIAA_ud.udThemeUrl + 'css/global.css">');
// document.write('<link type="text/css" rel="stylesheet" href="' + TIAA_ud.udThemeUrl + 'css/print_ud_atom.css" media="print">');
// document.write('<link type="text/css" rel="stylesheet" href="' + TIAA_ud.udThemeUrl + 'css/tablet_ud_atom.css" media="only screen and (max-device-width: 1024px)">');


// document.write('<script src="'+TIAA_ud.udThemeUrl+'js/ud_prototype_script.js'+'"></script>');