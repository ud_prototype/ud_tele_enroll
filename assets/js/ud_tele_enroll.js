$(document).on('bodyContLoaded', function(){
    // function getStore() {
    //     var __x = {};
    //     // Long loop only for IE6
    //     $.each(TIAA_ud.tempData, function(_x, _y) {
    //         __x[_x] = (store.get(_x) === undefined) ? '' : (store.get(_x)).replace(/"/ig, '');
    //     });
    //     return __x;
    // }

    var userInfo = store.get('user');

    if (userInfo === '' || userInfo === undefined) {
        store.set('user', 'exist');
        userInfo = getStore()['user'];
    };
    if (userInfo === 'exist') {
        $('#EnrollmentLookUp1').find('a').attr('class', 'collapsed');
        // $('div.panels').find('.hd:eq(0)').find('a').attr('class','collapsed');
        $('.proto_new').addClass('closed');
        $('.proto_existing').removeClass('closed');
    } else {
        $('.proto_new').removeClass('closed');
        $('.proto_existing').addClass('closed');
    };
    $('.today').text(Date.today().toString('MM/dd/yyyy'));
    $(".lastfivedays").text(Date.today().addDays(-5).toString('MM/dd/yyyy'));
    $(".lastsevendays").text(Date.today().addDays(-7).toString('MM/dd/yyyy'));

    //Hide DatePicker on Mouse Wheel scroll 
    $("body").bind('DOMMouseScroll mousewheel', function(e){
         $('#dob').datepicker("hide").blur();
    });
});
$(document).ready(function() {

$('#enrollBtn3').on('click', function(e){
		e.preventDefault();
		alert("Hellol");
	});

 $('.elookup').on('keyup', function() {
        var name = $(this).attr('name');
        if ($(this).val() !== '') {
            $(".elookup").attr("disabled", "disabled");
            $('input[name$=' + name + ']').removeAttr("disabled");
            $(this).focus();
        } else {
            $(".elookup").removeAttr("disabled");
        }
    });
    $('#resetEnb').on('click', function() {
        $(".elookup").removeAttr("disabled");
    });

    $pd = [{
        "ssn2_1": "123",
        "ssn2_2": "456",
        "ssn2_3": "7890",
        "f_name": "John",
        "l_name": "Smith",
        "email": "abcd@tiaa-cref.org",
        "dayphone1": "088",
        "dayphone2": "623",
        "dayphone3": "9786",
        "dob": "06/25/2013",
        "ma_line1": "7 Hills",
        "ma_city": "Boston",
        "ma_zip_1": "12345",
        "ma_zip_2": "9876"
    }];
    $('#elookup').on('click', function() {
        window.location.href = 'tele_enroll.html?user=exist';
    });
    $('#editInfo').on('click', function() {
        window.location.href = 'tele_enroll.html?user=new';
    });
    $('#sameaddress').on('click', function() {
        $.each($pd, function(index, obj) {
            $("#ra_line1").val(obj.ma_line1);
            $("#ra_city").val(obj.ma_city);
            $("#ra_zip_1").val(obj.ma_zip_1);
            $("#ra_zip_2").val(obj.ma_zip_2);
        });
    });
    $aac = [{
        "sn": "Indiana University",
        "a_code": "123456",
        "plans": "UoR 451P",
        "s_plans": "GS101I",
        "sp_name": "abcd"
    }, {
        "sn": "Cleveland State University",
        "a_code": "901011",
        "plans": "UoR 452P",
        "s_plans": "GS102I",
        "sp_name": "efgh"
    }, {
        "sn": "Oxford State University",
        "a_code": "131415",
        "plans": "UoR 453P",
        "s_plans": "GS103I",
        "sp_name": "ijkl"
    }, {
        "sn": "Columbia University",
        "a_code": "171819",
        "plans": "UoR 454P",
        "s_plans": "GS104I",
        "sp_name": "mnop"
    }, {
        "sn": "Hollins University",
        "a_code": "212223",
        "plans": "UoR 455P",
        "s_plans": "GS105I",
        "sp_name": "qrst"
    }, {
        "sn": "Kansas State University",
        "a_code": "252627",
        "plans": "UoR 456P",
        "s_plans": "GS106I",
        "sp_name": "uvwx"
    }, {
        "sn": "University of Kentucky",
        "a_code": "293031",
        "plans": "UoR 457P",
        "s_plans": "GS107I",
        "sp_name": "yzab"
    }, {
        "sn": "Iowa State University",
        "a_code": "333435",
        "plans": "UoR 458P",
        "s_plans": "GS108I",
        "sp_name": "abcd"
    }, {
        "sn": "Hollins University",
        "a_code": "373839",
        "plans": "UoR 459P",
        "s_plans": "GS109I",
        "sp_name": "ghij"
    }, {
        "sn": "Indiana Wesleyan University",
        "a_code": "414243",
        "plans": "UoR 450P",
        "s_plans": "GS110I",
        "sp_name": "klmn"
    }, {
        "sn": "University of Kentucky",
        "a_code": "454647",
        "plans": "UoR 461P",
        "s_plans": "GS111I",
        "sp_name": "opqr"
    }, {
        "sn": "The state college of Indiana",
        "a_code": "000123",
        "plans": "UoR 462P",
        "s_plans": "GS112I",
        "sp_name": "stuv"
    }, {
        "sn": "Indiana University of Pennsylvania",
        "a_code": "495051",
        "plans": "UoR 463P",
        "s_plans": "GS113I",
        "sp_name": "abcd"
    }];
    $('#eluBtn').on('click', function() {
        var $eluV = $("#school_name").val();
        var $tbody = $("#dataDisp");
        $tbody.html('');
        $.each($aac, function(index, obj) {
            if ($eluV.toLowerCase().match(obj.sp_name.toLowerCase())) {
                $tbody.append("<tr><td>" + obj.a_code + "</td><td>" + obj.plans + "</td><td>" + obj.s_plans + "</td><td>" + obj.sn + "</td></tr>");
            }
        });
    });

    $('#saveBtn').on('click', function(){
        $("#saveFormData").dialog('open');
    });

    $("#saveFormData").find('.btnBar button').on('click', function() {
        $("#saveFormData").dialog("close");
    });
   

    $('td.actionBtns').find('button').not('.btnOff').on('click', function(){ 
        var sf = $(this), 
            par = sf.parent(),
            process = par.find('.inProcess'),
            complete = par.find('.complete'),
            allActionBtns = $('td.actionBtns').find('button').not(':.btnOff').addClass('btnOff'),
            allUpdBtns = $('button.updateBtn').addClass('btnOff');

            sf.hide('fast', function(){
                process.removeClass('hidden');
            });

            setTimeout(function(){
                process.addClass('hidden');
                if(sf.next().prop("tagName").toLowerCase() === 'button'){
                    sf.next().removeClass('hidden');
                }else{
                    complete.removeClass('hidden');
                };

                if(complete.length <= 0){
                    sf.show();
                    sf.closest('tr').find('span.sdaDate').text(Date.today().toString('MM/dd/yyyy'));
                };

                allActionBtns.removeClass('btnOff');
                allUpdBtns.removeClass('btnOff');

            }, 2000);
    });
	

});