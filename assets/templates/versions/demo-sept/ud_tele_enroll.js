
var userInfo = store.get('user');
if (userInfo === '' || userInfo === undefined) {
    store.set('user', 'exist');
    userInfo = store.get('user');
};
            
$(document).on('bodyContLoaded', function(){

    function getStore() {
        var __x = {};
        // Long loop only for IE6
        $.each(TIAA_ud.tempData, function(_x, _y) {
            __x[_x] = (store.get(_x) === undefined) ? '' : (store.get(_x)).replace(/"/ig, '');
        });
        return __x;
    }

    // var userInfo = store.get('user');

    // if (userInfo === '' || userInfo === undefined) {
    //     store.set('user', 'exist');
    //     userInfo = store.get('user');
    // };

    if (userInfo === 'exist') {
        $('#EnrollmentLookUp1').find('a').attr('class', 'collapsed');
        // $('div.panels').find('.hd:eq(0)').find('a').attr('class','collapsed');
        $('.proto_new').addClass('closed');
        $('.proto_existing').removeClass('closed');
    } else {
        $('.proto_new').removeClass('closed');
        $('.proto_existing').addClass('closed');
    };
    $('.today').text(Date.today().toString('MM/dd/yyyy'));
    $(".lastfivedays").text(Date.today().addDays(-5).toString('MM/dd/yyyy'));
    $(".lastsevendays").text(Date.today().addDays(-7).toString('MM/dd/yyyy'));


    var flashMovie = null, flashMovie1 = null;
   function loadFlash(){
      flashMovie = $('#flashInteract .movie');
      flashMovie.flash({
            swf: 'assets/video/srkole_sda_video.swf',
            width: 800,
            height: 600,
            play: true
         });
    };
    function loadFlash1(){
        flashMovie1 = $('#flashInteract1 .movie');
        flashMovie1.flash({
            swf: 'assets/video/mvas_video.swf',
            width: 800,
            height: 600,
            play: true
         });
    };


   // loadFlash();
   // loadFlash1();

    // (function(){
        var $eBtn1 = $('#enrollBtn1'),
        $eBtn2 = $('#enrollBtn2'),
        $eBtn3 = $('#enrollBtn3'),
        $eBtn4 = $('#enrollBtn4'),
        $sdaBtn1 = $('#sdaBtn1'),
        $sdaBtn2 = $('#sdaBtn2'),
        $sdaBtn3 = $('#sdaBtn3'),
        $uptBtn1 = $('#uptBtn1'),
        $uptBtn2 = $('#uptBtn2'),
        $toShow = $('tr.showRow'),
        $toHide = $('tr.hideRow'),
        vdoFlag1 = false, 
        vdoFlag2 = false;

        // setTimeout(function() {
            $("#startVideo").on("dialogclose", function(event, ui) {
                // flashMovie.flash(
                //     function() {
                //         this.StopPlay();
                //     }
                // );
                $('#flashInteract .movie').html('');
            });

            $("#startVideo1").on("dialogclose", function(event, ui) {
                // flashMovie1.flash(
                //     function() {
                //         this.StopPlay();
                //     }
                // );
                $('#flashInteract1 .movie').html('');
            });

            // $('#pause1').on('click', function(){
            //     flashMovie.flash(
            //         function() {
            //             this.StopPlay();
            //         }
            //     );
            // });

        // }, 2000)

    $toShow.hide();

    $sdaBtn1.on('click', function(){
        $('#startVideo').dialog('open');
        loadFlash();
        vdoFlag1 = true;
    });

    $uptBtn1.on('click', function(){
        if($sdaBtn1.is(':visible') && vdoFlag1 === true){
            $.merge($sdaBtn1,$eBtn2).hide();
            $.merge($sdaBtn1.parent(), $eBtn2.parent()).find('span.inProcess').removeClass('hidden');
            if($eBtn2.parent().find('span.complete').is(':visible')){
                $eBtn2.parent().find('span.inProcess').addClass('hidden');
            };
            setTimeout(function(){
                $.merge($sdaBtn1.parent(), $eBtn2.parent()).find('span.inProcess').addClass('hidden');
                $.merge($sdaBtn1.parent(), $eBtn2.parent()).find('span.complete').removeClass('hidden');
            }, 2000);
        }
    });



    $.merge($sdaBtn2,$sdaBtn3).on('click', function(){
        $('#startVideo1').dialog('open');
        loadFlash1();
         vdoFlag2 = true;
    });

    $uptBtn2.on('click', function(){
        // if($eBtn3.is(':visible') && vdoFlag2 === true){
            $.merge($sdaBtn2,$sdaBtn3).hide();
            $.merge($sdaBtn2.parent(), $sdaBtn3.parent()).find('span.inProcess').removeClass('hidden');
            setTimeout(function(){
                $toHide.hide();
                $.merge($sdaBtn3.parent(), $sdaBtn2.parent()).find('span.inProcess').addClass('hidden');
                $sdaBtn3.hide();$eBtn4.removeClass('hidden');
                $toShow.show();
                // $.merge($sdaBtn3.parent(), $eBtn3.parent()).find('span.complete').removeClass('hidden');
            }, 2000);
        // };
    });


    $('td.actionBtns').find('button').not('.btnOff').not('.noFunc').on('click', function(){ 
    var sf = $(this), 
        par = sf.parent(),
        process = par.find('.inProcess'),
        complete = par.find('.complete'),
        allActionBtns = $('td.actionBtns').find('button').not(':.btnOff').addClass('btnOff'),
        allUpdBtns = $('button.updateBtn').addClass('btnOff');

        sf.hide('fast', function(){
            process.removeClass('hidden');
        });

        setTimeout(function(){
            process.addClass('hidden');
            if(sf.next().prop("tagName").toLowerCase() === 'button'){
                sf.next().removeClass('hidden');
            }else{
                complete.removeClass('hidden');
            };

            if(complete.length <= 0){
                sf.show();
                sf.closest('tr').find('span.sdaDate').text(Date.today().toString('MM/dd/yyyy'));
            };

            allActionBtns.removeClass('btnOff');
            allUpdBtns.removeClass('btnOff');

        }, 2000);
    });
        
    // })();

    //Hide DatePicker on Mouse Wheel scroll 
    $("body").bind('DOMMouseScroll mousewheel', function(e){
         $('#dob').datepicker("hide").blur();
    });

});


$(document).ready(function() {

    $('.elookup').on('keyup', function() {
        var name = $(this).attr('name');
        if ($(this).val() !== '') {
            $(".elookup").attr("disabled", "disabled");
            $('input[name$=' + name + ']').removeAttr("disabled");
            $(this).focus();
        } else {
            $(".elookup").removeAttr("disabled");
        }
    });
    $('#resetEnb').on('click', function() {
        $(".elookup").removeAttr("disabled");
    });

    



});