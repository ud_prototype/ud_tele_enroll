

$(document).on('bodyContLoaded', function () {
	
    (sp8Scn === 'ssnNo') ?  store.set('user', 'new'): store.set('user', 'exist');
    userInfo = store.get('user');

    $('.today').text(Date.today().toString('MM/dd/yyyy'));
    $(".lastfivedays").text(Date.today().addDays(-5).toString('MM/dd/yyyy'));
    $(".lastsevendays").text(Date.today().addDays(-7).toString('MM/dd/yyyy'));
    
    $('#ParticipantDetails1').find('a').attr('class', 'collapsed');
    $('#EligiblePlans1').find('a').attr('class', 'collapsed');
    $('#ActivePlans1').find('a').attr('class', 'collapsed');
    $('#lookupForm2, #lookupTable').addClass('closed');

    if (sp8Scn === 'ssnYes') {
        $('#EnrollmentLookUp1').find('a').attr('class', 'collapsed');
        // $('div.panels').find('.hd:eq(0)').find('a').trigger('click');
        $('#ParticipantDetails1').find('a').attr('class', 'expanded');
        $('#EligiblePlans1').find('a').attr('class', 'expanded');
        $('.proto_new').addClass('closed');
        $('.proto_existing').removeClass('closed');
        $('.noDataEligTable').addClass('closed')
    } else {
        // $('#ParticipantDetails1').find('a').attr('class', 'collapsed');
        // $('#EligiblePlans1').find('a').attr('class', 'collapsed');
        // $('#ActivePlans1').find('a').attr('class', 'collapsed');
        $('.proto_new').removeClass('closed');
        $('.proto_existing').addClass('closed');
        $('.dataEligTable').addClass('closed');
    };

    // $('.pageTitleUD').find('.proto_new').removeClass('closed');
    // $('.pageTitleUD').find('.proto_existing').addClass('closed');


    $('#elookup').on('click', function(){
        if (sp8Scn === 'ssnNo') {
            $('#lookupForm1').addClass('closed');
            $('#lookupForm2').removeClass('closed');
            $('#mainAlert').slideDown().delay(1000).slideUp();
        }else{
            $('#EnrollmentLookUp1').find('a').trigger('click');
            $('#ParticipantDetails1').find('a').trigger('click');
            $('#EligiblePlans1').find('a').trigger('click');
            $('#ActivePlans1').find('a').trigger('click');
            $('.pageTitleUD').find('.proto_new').addClass('closed');
            $('.pageTitleUD').find('.proto_existing').removeClass('closed');
        };
    });

    var addFlag = false;
    $('#saveBtn').on('click', function () {
        if(!addFlag){
            $("#saveFormData").dialog('open');
            addFlag = true;
        }else{
            $('#EnrollmentLookUp1').find('a').hasClass('expanded') && $('#EnrollmentLookUp1').find('a').trigger('click');
            $('#EligiblePlans1').find('a').hasClass('collapsed') && $('#EligiblePlans1').find('a').trigger('click');
            $('#ActivePlans1').find('a').hasClass('collapsed') && $('#ActivePlans1').find('a').trigger('click');
            $('.proto_new').addClass('closed');
            $('.proto_existing').removeClass('closed');
            $('.dataEligTable').removeClass('closed')
            $('.noDataEligTable').addClass('closed')
        };
    });
        var clientName = $('#clientName').html();
        $('#clientName').html('');

        $('#lookupForm2').on('submit', function(){var newTimer, newTimer1, newTimer2, thisVal = $('#accessCode1').val();
            // clearTimeout(newTimer); 
            // clearTimeout(newTimer2);
            // $('#lookupBtns').find('input:eq(0)').addClass('btnOff');
            // $('#clientName').html('');
            // $('#lookupTable').addClass('closed');
            // $('#lookupTableNoData').removeClass('closed'); 
            clearTimeout(newTimer1);  
            // newTimer = setTimeout(function(){
                    if(thisVal.length > 0 && /^[a-zA-Z0-9- ]*$/.test(thisVal)){
                        $('#lookupProcess').css('visibility', 'visible');
                        clearTimeout(newTimer1);
                        newTimer1 = setTimeout(function(){
                             $('#lookupTableNoData').addClass('closed');
                             $('#lookupTable').removeClass('closed');
                             $('#lookupBtns').find('input').removeClass('btnOff');
                             $('#lookupProcess').css('visibility', 'hidden');
                             $('#clientName').html(clientName);
                             $('#subAlert1').hide()
                        }, 1000);
                    }else{
                        $('#subAlert1').show();
                        $('#lookupBtns').find('input:eq(0)').addClass('btnOff');
            $('#clientName').html('');
            $('#lookupTable').addClass('closed');
            $('#lookupTableNoData').removeClass('closed'); 
                    };
                // }, 1000);
                return false;

        });


        $('#lookupNextBtn').on('click', function(){
            if(userInfo === 'new'){
                $('#EnrollmentLookUp1').find('a').hasClass('expanded') && $('#EnrollmentLookUp1').find('a').trigger('click');
                $('#ParticipantDetails1').find('a').hasClass('collapsed') && $('#ParticipantDetails1').find('a').trigger('click');
                $('#EligiblePlans1').find('a').hasClass('expanded') && $('#EligiblePlans1').find('a').trigger('click');
                $('#ActivePlans1').find('a').hasClass('expanded') && $('#ActivePlans1').find('a').trigger('click');
            };
        });

        $('#lookupCancelBtn').on('click', function(){
            $('#lookupForm2').addClass('closed');
            $('#lookupForm1').removeClass('closed');
            $('#ParticipantDetails1').find('a').hasClass('expanded') && $('#ParticipantDetails1').find('a').trigger('click');
            $('#EligiblePlans1').find('a').hasClass('expanded') && $('#EligiblePlans1').find('a').trigger('click');
            $('#ActivePlans1').find('a').hasClass('expanded') && $('#ActivePlans1').find('a').trigger('click');
        });    



    // $('td.actionBtns').find('button').not('.btnOff').on('click', function () {
    //     localStorage.btnChange = "1";
    //     var sf = $(this),
    //         par = sf.parent(),
    //         process = par.find('.inProcess'),
    //         complete = par.find('.complete'),
    //         allActionBtns = $('td.actionBtns').find('button').not(':.btnOff').addClass('btnOff'),
    //         allUpdBtns = $('button.updateBtn').addClass('btnOff');
    //     sf.hide('fast', function () {
    //         process.removeClass('hidden');
    //     });
    //     setTimeout(function () {
    //         process.addClass('hidden');
    //         if (sf.next().prop("tagName").toLowerCase() === 'button') {
    //             sf.next().removeClass('hidden');
    //         } else {
    //             complete.removeClass('hidden');
    //         };
    //         if (complete.length <= 0) {
    //             sf.show();
    //             sf.closest('tr').find('span.sdaDate').text(Date.today().toString('MM/dd/yyyy'));
    //         };
    //         allActionBtns.removeClass('btnOff');
    //         allUpdBtns.removeClass('btnOff');
    //     }, 2000);
    // });



     var $eBtn1 = $('#enrollBtn1'),
        $eBtn2 = $('#enrollBtn2'),
        $eBtn3 = $('#enrollBtn3'),
        $eBtn4 = $('#enrollBtn4'),
        $sdaBtn1 = $('#sdaBtn1'),
        $sdaBtn2 = $('#sdaBtn2'),
        $sdaBtn3 = $('#sdaBtn3'),
        $uptBtn1 = $('#uptBtn1'),
        $uptBtn2 = $('#uptBtn2'),
        $toShow = $('.showActiveTable'),
        $toHide = $('.hideActiveTable'),
        vdoFlag1 = false,
        vdoFlag2 = false;

        $toShow.hide();

    // $sdaBtn1.on('click', function(){
    //     vdoFlag1 = true;
    // });
    $.merge($sdaBtn2,$sdaBtn3).on('click', function(){
         vdoFlag2 = true;
    });

    //  $uptBtn1.on('click', function(){
    //     if(vdoFlag1 === true){
    //         $.merge($sdaBtn1,$eBtn2).hide();
    //         $.merge($sdaBtn1.parent(), $eBtn2.parent()).find('span.inProcess').removeClass('hidden');
    //         if($eBtn2.parent().find('span.complete').is(':visible')){
    //             $eBtn2.parent().find('span.inProcess').addClass('hidden');
    //         };
    //         setTimeout(function(){
    //             $.merge($sdaBtn1.parent(), $eBtn2.parent()).find('span.inProcess').addClass('hidden');
    //             $.merge($sdaBtn1.parent(), $eBtn2.parent()).find('span.complete').removeClass('hidden');
    //         }, 2000);
    //     }
    // });

     $uptBtn2.on('click', function(){
        if(vdoFlag2 === true){
            $.merge($sdaBtn2,$sdaBtn3).hide();
            $.merge($eBtn3,$eBtn4).hide();
            $.merge($sdaBtn2.parent(), $sdaBtn3.parent()).find('span.inProcess').removeClass('hidden');
            setTimeout(function(){
                $toHide.hide();
                $('#ActivePlans1').find('a').trigger('click');
                $('.hideRow').addClass('closed');
                $($eBtn3).show();
                $.merge($sdaBtn3.parent(), $sdaBtn2.parent()).find('span.inProcess').addClass('hidden');
                $sdaBtn3.hide();$eBtn4.removeClass('hidden');
                $toShow.show();
                // $.merge($sdaBtn3.parent(), $eBtn3.parent()).find('span.complete').removeClass('hidden');
            }, 2000);
        };
    });
    
    $('.elookup').on('keyup', function () {
        var name = $(this).attr('name');
        if ($(this).val() !== '') {
            $(".elookup").attr("disabled", "disabled");
            $('input[name$=' + name + ']').removeAttr("disabled");
            $(this).focus();
        } else {
            $(".elookup").removeAttr("disabled");
        }
    });
    $('#resetEnb').on('click', function () {
        $(".elookup").removeAttr("disabled");
    });
    $pd = [{
        "ssn2_1": "123",
        "ssn2_2": "456",
        "ssn2_3": "7890",
        "f_name": "John",
        "l_name": "Smith",
        "email": "abcd@tiaa-cref.org",
        "dayphone1": "088",
        "dayphone2": "623",
        "dayphone3": "9786",
        "dob": "06/25/2013",
        "ma_line1": "7 Hills",
        "ma_city": "Boston",
        "ma_zip_1": "12345",
        "ma_zip_2": "9876"
    }];
    // $('#elookup').on('click', function () {
    //     window.location.href = 'tele_enroll.html?user=exist';
    // });
    // $('#editInfo').on('click', function () {
    //     window.location.href = 'tele_enroll.html?user=new';
    // });
    $('#sameaddress').on('click', function () {
        $.each($pd, function (index, obj) {
            $("#ra_line1").val(obj.ma_line1);
            $("#ra_city").val(obj.ma_city);
            $("#ra_zip_1").val(obj.ma_zip_1);
            $("#ra_zip_2").val(obj.ma_zip_2);
        });
    });

    $("#saveFormData").find('.btnBar button').on('click', function () {
        $("#saveFormData").dialog("close");
    });
    var popFlag = false;
    $('.popBtn').on('click', function(){
        popFlag = true;
    });
    $('td.actionBtns').find('button').not('.btnOff').on('click', function () {
        // localStorage.btnChange = "1";
      //  if($(this).hasClass('btn2')){

        switch(this.id){
            case 'enrollBtn3':  $("#saveResidentialAdd").dialog('open');
                                $(".addrFields").hide();
                                $("#USA").show();
                                $("#RA_Country1").on('change',function(){
                                    var country=$(this).val();
                                    $(".addrFields").hide();
                                    $('#'+country).show();
                                });
                                $("#saveResidentialAdd .btn, #saveResidentialAdd .btn3").on('click',function(){
                                    $("#saveResidentialAdd").dialog("close");
                                });
                                break;
            case 'enrollBtn4':  $('#verification').dialog("open");
                                 $("#verification .btn,#verification .btn3").on('click',function(){
                                    $("#verification").dialog("close");
                                });
                                break;    
        }

        var sf = $(this),
            par = sf.parent(),
            process = par.find('.inProcess'),
            complete = par.find('.complete'),
            allActionBtns = $('td.actionBtns').find('button').not(':.btnOff').addClass('btnOff'),
            allUpdBtns = $('button.updateBtn').addClass('btnOff');
            sf.hide('fast', function () {
                process.removeClass('hidden');
            });
        setTimeout(function () {
            process.addClass('hidden');
            if (sf.next().prop("tagName").toLowerCase() === 'button') {
                sf.next().removeClass('hidden');
            } else {
                complete.removeClass('hidden');
            };
            if (complete.length <= 0) {
                sf.show();
                sf.closest('tr').find('span.sdaDate').text(Date.today().toString('MM/dd/yyyy'));
            };
            allActionBtns.removeClass('btnOff');
            allUpdBtns.removeClass('btnOff');
        }, 2000);
    });


    $('#disclosure-verification').on('change',function(){
        if ($(this).is(':checked')){
            $('#saveAddress').removeClass('btnOff');
        }
        else{
            $('#saveAddress').addClass('btnOff');
        }
    });

    $('#chkverification').on('change',function(){
        if ($(this).is(':checked')){
            $('#saveVerification').removeClass('btnOff');
        }
        else{
            $('#saveVerification').addClass('btnOff');
        }
    });

    //Hide DatePicker on Mouse Wheel scroll 
    $("body").bind('DOMMouseScroll mousewheel', function(e){
         $('#dob').datepicker("hide").blur();
    });

});

