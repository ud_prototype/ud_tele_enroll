

$(document).on('bodyContLoaded', function () {
	
	sp8Scn='';

	var sp18Scn=store.get('sp18-scenario')===undefined?'fastLane':store.get('sp18-scenario');

	var enrollDiscFlg=false;

	(sp8Scn === 'ssnNo') ?  store.set('user', 'new'): store.set('user', 'exist');
	userInfo = store.get('user');

	$('.today').text(Date.today().toString('MM/dd/yyyy'));
	$(".lastfivedays").text(Date.today().addDays(-5).toString('MM/dd/yyyy'));
	$(".lastsevendays").text(Date.today().addDays(-7).toString('MM/dd/yyyy'));
	
/*Sp18 Scenario*/
	if(sp18Scn=="noFastLane" || sp18Scn=="fastLane" || sp18Scn=="noFastLaneNoSDA"){
		$('#EnrollmentLookUp1 a,#ParticipantDetails1 a').attr('class', 'collapsed');
		$("#EligiblePlans1 a,#ActivePlans1 a,#openEnrollment a").trigger("click");
		$('#lookupForm2, #lookupTable').addClass('closed');
		$('.noDataEligTable,.elg-plans').addClass('closed');
		if(sp18Scn=="fastLane"){ 
			$("#fastLaneSec, #fastLaneTbl").removeClass("closed");
			$("#fastLaneSec1").removeClass("closed");
			$("#dataDisplay,#dataDisplay1").removeClass("closed");
		}

	    if(sp18Scn=="noFastLane"){ 
			$("#noFastLaneSDA,#noFastLaneTIAA").removeClass("closed");
		    $("#nodataDisplay,#nodataDisplay1").removeClass("closed");
            $("#activePlanTbl tr.hiderow").removeClass("closed");	
            $("#activePlanTbl tr.planInfo").addClass("closed");			
				
		}
		if(sp18Scn=="noFastLaneNoSDA"){ 
			$("#noFastLaneNoSDASec, #noFastLaneExt").removeClass("closed");
			$("#nodataDisplay,#nodataDisplay1").removeClass("closed");
		}
	
		
	}
	else{
		$('#ParticipantDetails1 a,#EligiblePlans1 a,#ActivePlans1 a,#openEnrollment a').attr('class', 'collapsed');
		$('#lookupForm2, #lookupTable').addClass('closed');
	}
	if (sp8Scn === 'ssnYes') {
		$('#EnrollmentLookUp1 a').attr('class', 'expanded');
		$('#ParticipantDetails1 a,#EligiblePlans1 a,#ActivePlans1 a').attr('class', 'collapsed');
		$('.proto_new').addClass('closed');
		$('.proto_existing').removeClass('closed');
		$('.noDataEligTable').addClass('closed')
		//enrollDiscFlg=true;
		$("#res-add-sec").removeClass("hidden");

	} else if(sp8Scn === 'ssnNo') {
		// $('#ParticipantDetails1').find('a').attr('class', 'collapsed');
		// $('#EligiblePlans1').find('a').attr('class', 'collapsed');
		// $('#ActivePlans1').find('a').attr('class', 'collapsed');
		$('.proto_new').removeClass('closed');
		$('.proto_existing').addClass('closed');
		$('.dataEligTable').addClass('closed');
	};
	var urlN = location.href.substring(location.href.lastIndexOf('#') + 1);
	if(urlN == 'enroll'){
		$('#nodataDisplay').removeClass('closed');
		$('#dataDisplay').addClass('closed');
		$('#nodataDisplay1').removeClass('closed');
		$('#dataDisplay1').addClass('closed');
	}else if(urlN == 'modify'){
		$('#nodataDisplay').addClass('closed');
		$('#dataDisplay').removeClass('closed');
		$('#nodataDisplay1').addClass('closed');
		$('#dataDisplay1').removeClass('closed');
	}
	$(document).on('click', '#viewDetails', function(e){
		e.preventDefault();
		if($('#modifySDA2').hasClass('hidden') == true){
			location.href = 'plan1tele_enroll.html#enroll';
		}else{
			location.href = 'plan1tele_enroll.html#modify';
		}
	});
	
	
	
	
	$('#elookup').on('click', function(){
		var ssn01 = $('#ssn').val(),
			ssn02 = $('#ssn1').val(),
			ssn03 = $('#ssn2').val();
		if (sp8Scn === 'ssnNo') {
			$('#lookupForm1').addClass('closed');
			$('#lookupForm2').removeClass('closed');
			$('#mainAlert').slideDown();
		}else{
			$('#EnrollmentLookUp1').find('a').trigger('click');
			$('#ParticipantDetails1').find('a').trigger('click');
			$('#EligiblePlans1').find('a').trigger('click');
			//$('#ActivePlans1').find('a').trigger('click');
			$('.pageTitleUD').find('.proto_new').addClass('closed');
			$('.pageTitleUD').find('.proto_existing').removeClass('closed');
		};
		$('#ssn2_1').val((ssn01==""||ssn01==null)?"123":ssn01);
		$('#ssn2_2').val((ssn02==""||ssn02==null)?"12":ssn02);
		$('#ssn2_3').val((ssn03==""||ssn03==null)?"1234":ssn03);
		$("#lblSSN").html(((ssn01==""||ssn01==null)?"123":ssn01)+'-'+((ssn02==""||ssn02==null)?"12":ssn02)+'-'+((ssn03==""||ssn03==null)?"1234":ssn03));
	});

	var addFlag = false,
		saveBtnCtr=0;
			
			
	$('#saveBtn').on('click', function () {
	var dayphoneno = "(" + $('#dayphone1').val() + ")" + " " +  $('#dayphone2').val() + " - " + $('#dayphone3').val(),
	evephoneno = "(" + $('#evephone1').val() + ")" + " " +  $('#evephone2').val() + " - " + $('#evephone3').val(),
	ssnno = $('#ssn2_1').val() + "-" +  $('#ssn2_2').val() + "-" + $('#ssn2_3').val(),
	dob = $('#dob').val(),
	email = $('#email').val(),
	gender = $('#Gender').val(),
	empid = $('#empid').val(),
	mailAdd= $('#ma_line1').val()+[($('#ma_line2').val()!="")?'<br/>'+$('#ma_line2').val():''],
	mailState = $('#ma_city').val()+', '+$('#ma_state').val(),
	mailCountry = $('#MA_Country').val(),
	zipcode = $('#ma_zip_1').val();
	$("#Pdalerts,#Pdalerts li").addClass("hidden").removeClass("visible");
	$("#defaultform [aria-required=true]").each(function(){
				var pDetailsIp=$("#"+this.id),
					pDetailsIpVal=$.trim(pDetailsIp.val());
					pDetailsErr=$("#"+this.id+"-err");
				if(pDetailsIpVal==0 || typeof pDetailsIpVal == "undefined" || pDetailsIpVal =="none" || pDetailsIpVal=="" || pDetailsIpVal=="mm/dd/yyyy"){
					if(pDetailsErr.length>0){
						pDetailsErr.removeClass('hidden');
						saveBtnCtr=1;
					}
				}
				else{
					pDetailsErr.addClass('hidden');
				}
			});

		if(saveBtnCtr==1){
			$('#Pdalerts').removeClass('hidden').addClass("visible");
			saveBtnCtr=0;
		}
		else if(!addFlag && saveBtnCtr==0){
			$("#Pdalerts,#Pdalerts li").addClass("hidden").removeClass("visible");
			
			$("#wrapper").find('#Pdalerts').addClass('hidden').removeClass('visible');
			$("#mAddress,#uspsAddress").html(mailAdd + "<br/>" + mailState + " " + zipcode+"<br/>"+mailCountry);
			$("#saveFormData").dialog('open');
			addFlag = true;
			$('.saveadd').on ('click',function(){
				$("#saveFormData").dialog('close');
				$('#EnrollmentLookUp1').find('a').hasClass('expanded') && $('#EnrollmentLookUp1').find('a').trigger('click');
				$('#EligiblePlans1').find('a').hasClass('collapsed') && $('#EligiblePlans1').find('a').trigger('click');
				$("#openEnrollment a").hasClass('collapsed') && $("#openEnrollment a").trigger('click');
				//$('#ActivePlans1').find('a').hasClass('collapsed') && $('#ActivePlans1').find('a').trigger('click');
				$('.proto_new').addClass('closed');
				$('#lblSSN').html(ssnno);
				$('#lblmailing_address').html(mailAdd + "<br/>" + mailState + " " + zipcode+"<br/>"+mailCountry);
				$('#lbldaytime_phone').html(dayphoneno);
				$('#lblevening_phone').html(evephoneno);
				$('#lblDOB').html(dob);
				$('#lblEmail').html(email);
				$('#lblgender').html(gender);
                $('#lblempid').html(empid);	
                $('#lblresidential_address').html(mailAdd + "<br/>" + mailState + " " + zipcode+"<br/>"+mailCountry);								
				$('.proto_existing').removeClass('closed');
				$('.dataEligTable').removeClass('closed');
				$('.noDataEligTable').addClass('closed');
			
		});
		}else if(!saveBtnCtr){
		
		};
	});

		$("#accessCode1").on('keyup blur',function(){   
			var aCodeObj=$("#searchAccess");
			($(this).val().length>0)?aCodeObj.removeClass("btnOff"):aCodeObj.addClass("btnOff");
		})

		var clientName = $('#clientName').html();
		$('#clientName').html('');
		var clientName1 = $('#clientName1').html();
		$('#clientName1').html('');
		var clientName2 = $('#clientName2').html();
		$('#clientName2').html('');

		$('#searchAccess').on('click', function(){var newTimer, newTimer1, newTimer2, thisVal = $('#accessCode1').val();
			clearTimeout(newTimer1);  
			// newTimer = setTimeout(function(){
				if(thisVal.length > 0 && /^[a-zA-Z0-9- ]*$/.test(thisVal)) {
					switch(thisVal){
						case '101010a':
							$('#lookupProcess').css('visibility', 'visible');
							clearTimeout(newTimer1);
							newTimer1 = setTimeout(function(){
								 $('#lookupTableNoData,#lookupTable,#lookupTable2,.cName,.cName2').addClass('closed');
								 $('#lookupTable1,.cName1').removeClass('closed');
								 $('#lookupBtns').find('input').removeClass('btnOff');
								 $('#lookupProcess').css('visibility', 'hidden');
								 $('#clientName1').html(clientName1);
								 $('#subAlert1').hide()
							}, 1000);
							$(".elg-plans").addClass("closed");
							$("#eplans-sec1,#eplans-sec2a").removeClass("closed");
							break;
						case '405800':
							$('#lookupProcess').css('visibility', 'visible');
							clearTimeout(newTimer1);
							newTimer1 = setTimeout(function(){
								 $('#lookupTableNoData,#lookupTable,#lookupTable1,.cName,.cName1').addClass('closed');
								 $('#lookupTable2,.cName2').removeClass('closed');
								 $('#lookupBtns').find('input').removeClass('btnOff');
								 $('#lookupProcess').css('visibility', 'hidden');
								 $('#clientName2').html(clientName2);
								 $('#subAlert1').hide()
							}, 1000);
							$(".elg-plans").addClass("closed");
							$("#eplans-sec1,#eplans-sec2b").removeClass("closed");
							break;
						case '151212':
							$('#lookupProcess').css('visibility', 'visible');
							clearTimeout(newTimer1);
							newTimer1 = setTimeout(function(){
								 $('#lookupTableNoData,#lookupTable1,#lookupTable2,.cName1,.cName2').addClass('closed');
								 $('#lookupTable,.cName').removeClass('closed');
								 $('#lookupBtns').find('input').removeClass('btnOff');
								 $('#lookupProcess').css('visibility', 'hidden');
								 $('#clientName').html(clientName);
								 $('#subAlert1').hide()
							}, 1000);
							$(".elg-plans").addClass("closed");
							$("#eplans-sec1").removeClass("closed");
							break;
						case '406081':
							$(".elg-plans").addClass("closed");
							$("#eplans-sec1,#eplans-sec2").removeClass("closed");
							break;
						default:
							$("#err-acc-code").html($("#accessCode1").val());
							$('#subAlert1').show();
							$('#lookupBtns').find('input:eq(0)').addClass('btnOff');
							$('#clientName,#clientName1,#clientName2').html('');
							$('#lookupTable,#lookupTable1').addClass('closed');
							$('#lookupTableNoData').removeClass('closed');  
							break;
					}
				}

	 		return false;

		});


		$('#lookupNextBtn').on('click', function(){
			if(userInfo === 'new'){
				//$('#EnrollmentLookUp1').find('a').hasClass('expanded') && $('#EnrollmentLookUp1').find('a').trigger('click');
				$('#ParticipantDetails1').find('a').hasClass('collapsed') && $('#ParticipantDetails1').find('a').trigger('click');
				$('#EligiblePlans1').find('a').hasClass('expanded') && $('#EligiblePlans1').find('a').trigger('click');
				$('#ActivePlans1').find('a').hasClass('expanded') && $('#ActivePlans1').find('a').trigger('click');
				$("#mainAlert").hide();
			};
		});

		$('#lookupCancelBtn').on('click', function(){
			$('#lookupForm2').addClass('closed');
			$('#lookupForm1').removeClass('closed');
			$('#ParticipantDetails1').find('a').hasClass('expanded') && $('#ParticipantDetails1').find('a').trigger('click');
			$('#EligiblePlans1').find('a').hasClass('expanded') && $('#EligiblePlans1').find('a').trigger('click');
			$('#ActivePlans1').find('a').hasClass('expanded') && $('#ActivePlans1').find('a').trigger('click');

			$('#lookupTable').addClass('closed');
			$('#lookupTableNoData').removeClass('closed'); 
			$("#accessCode1").val('');
			$.merge($("#lookupNextBtn"),$("#searchAccess")).addClass("btnOff");
			$("#mainAlert").hide();
		});    

	 var $eBtn1 = $('#enrollBtn1'),
		$eBtn2 = $('#enrollBtn2'),
		$eBtn3 = $('#enrollBtn3'),
		$eBtn4 = $('#enrollBtn4'),
		$eBtn5 = $('#enrollBtn5'),
		$eBtn6 = $('#enrollBtn6'),
		$eBtn7 = $('#enrollBtn7'),
		$sdaBtn1 = $('#sdaBtn1'),
		$sdaBtn2 = $('#sdaBtn2'),
		$sdaBtn3 = $('#sdaBtn3'),
		$sdaBtn4 = $('#sdaBtn4'),
		$sdaBtn5 = $('#sdaBtn5'),
		$sdaBtn6 = $('#sdaBtn6'),
		$sdaBtn7 = $('#sdaBtn7'),
		$uptBtn1 = $('#uptBtn1'),
		$uptBtn2 = $('#uptBtn2'),
		$uptBtn3 = $('#uptBtn3'),
		$toShow = $('.showActiveTable'),
		$toHide = $('.hideActiveTable'),
		$modifySda1 = $("#modify-sda-1"),
		$modifySda = $("#modifySDA"),
		$modifySda3 = $("#modifySDA3"),
		vdoFlag1 = false,
		vdoFlag2 = false;

		$toShow.hide();

	 $uptBtn1.on('click', function(){
		if(vdoFlag1 === true && enrollDiscFlg){
			$.merge($sdaBtn1,$sdaBtn2).hide();
			$.merge($eBtn1,$eBtn2).hide();
			$.merge($sdaBtn1.parent(), $sdaBtn2.parent()).find('span.inProcess').removeClass('hidden');
			setTimeout(function(){
				$toHide.hide();
				$('#ActivePlans1').find('a').hasClass('collapsed') && $('#ActivePlans1').find('a').trigger('click');	
				$('#eplans-sec1 .hideRow').addClass('closed');
				$('#elgbPlnsAdd .complete').removeClass('hidden');
				$.merge($sdaBtn1.parent(), $sdaBtn2.parent()).find('span.inProcess').addClass('hidden').remove();
				$eBtn2.removeClass('hidden');
				$toShow.show();
				$('#activePlanTbl .row1').removeClass("closed");
				// $.merge($sdaBtn3.parent(), $eBtn3.parent()).find('span.complete').removeClass('hidden');
			}, 2000);
			
		};
	});

	 $("#uptBtn6").on('click', function(){
		///if(vdoFlag1 === true && enrollDiscFlg){
			$("#enrollBtn12").hide().next('span.inProcess').removeClass('hidden');
			setTimeout(function(){
				$toHide.hide();
				$('#ActivePlans1').find('a').hasClass('collapsed') && $('#ActivePlans1').find('a').trigger('click');	
				$('#eplans-sec1 .hideRow').addClass('closed');
				//$('##eplans-sec1 .complete').removeClass('hidden');
				//$.merge($sdaBtn1.parent(), $sdaBtn2.parent()).find('span.inProcess').addClass('hidden').remove();
				//$eBtn2.removeClass('hidden');
				$toShow.show();
				$('#activePlanTbl .row1').removeClass("closed");
				// $.merge($sdaBtn3.parent(), $eBtn3.parent()).find('span.complete').removeClass('hidden');
			}, 2000);
		//};
	});


	 $uptBtn2.on('click', function(){ 
	 	var curAccCode=$("#accessCode1").val();
	 	$("#eplans-sec1").removeClass("closed");
			switch(curAccCode){
				case '101010a':$("#eplans-sec2a").removeClass("closed");
						break;
				case '405800':$("#eplans-sec2b").removeClass("closed");
						break;
				default:break;
			}
		if(vdoFlag2 == true && enrollDiscFlg){
			$.merge($sdaBtn3,$sdaBtn4).hide();
			$.merge($eBtn3,$eBtn4,$eBtn5,$eBtn6).hide();
			$.merge($sdaBtn3.parent(), $sdaBtn4.parent()).find('span.inProcess').removeClass('hidden');
			setTimeout(function(){
				//$toHide.hide();
				//$('#ActivePlans1').find('a').hasClass('collapsed') && $('#ActivePlans1').find('a').trigger('click');	
				//$('#elgbPlnsAdd .hideRow').removeClass('closed');
				$('#aCode').addClass("hidden");
				$('#accessCode').val('');
				$('#eplans-sec3').removeClass("closed");
				$("#elgbPlns .complete").removeClass("hidden");
				$("#elgbPlnsAdd1 .complete").removeClass("hidden");
				$("#elgbPlnsAdd1 .hideRow").addClass("closed");
				$.merge($sdaBtn3.parent(), $sdaBtn4.parent()).find('span.inProcess').addClass('hidden').remove();
				$eBtn5.removeClass('hidden');
				$toShow.show();
				$('#activePlanTbl .row2,#elgbPlns .no-data').removeClass("closed");
			}, 2000);
		};
	});
	 $uptBtn3.on('click', function(){
	 	
		if(vdoFlag2 == true && enrollDiscFlg){
			var curAccCode=$("#accessCode1").val();
			$("#eplans-sec1").removeClass("closed");
			switch(curAccCode){
				case '101010a':$("#eplans-sec2a").removeClass("closed").insertAfter($("#eplans-sec2b"));
						break;
				case '405800':$("#eplans-sec2b").removeClass("closed");
						break;
				default:break;
			}
		};
	});
	
	$('.elookup').on('keyup', function () {
		var name = $(this).attr('name');
		if ($(this).val() !== '') {
			$(".elookup").attr("disabled", "disabled");
			$('input[name$=' + name + ']').removeAttr("disabled");
			$(this).focus();
		} else {
			$(".elookup").removeAttr("disabled");
		}
	});
	$('#resetEnb').on('click', function () {
		$(".elookup").removeAttr("disabled");
	});
	
	$('#sameaddress').on('click', function () {
		$pd = [{
			"ssn2_1": "123",
			"ssn2_2": "456",
			"ssn2_3": "7890",
			"f_name": "John",
			"l_name": "Smith",
			"email": "abcd@tiaa-cref.org",
			"dayphone1": "088",
			"dayphone2": "623",
			"dayphone3": "9786",
			"dob": "06/25/1954",
			"ma_line1": $("#ma_line1").val(),
			"ma_city": $("#ma_city").val(),
			"ma_zip_1": $("#ma_zip_1").val(),
			"ma_zip_2": $("#ma_zip_2").val(),
			"ma_state": $("#ma_state").val()
		}];

		$.each($pd, function (index, obj) {
			$("#ra_line1").val(obj.ma_line1);
			$("#ra_city").val(obj.ma_city);
			$("#ra_zip_1").val(obj.ma_zip_1);
			$("#ra_zip_2").val(obj.ma_zip_2);
			$("#ra_state option[value='"+obj.ma_state+"']").attr('selected', 'selected');
		});
	});

	$("#saveFormData").find('.btnBar button').on('click', function () {
		$("#saveFormData").dialog("close");
	});
	var popFlag = false;
	$('.popBtn').on('click', function(){
		popFlag = true;
	});


	$('td.actionBtns').find('button').not('.btnOff').on('click', function () {
		// localStorage.btnChange = "1";
	  //  if($(this).hasClass('btn2')){
		if(this.id!="sdaBtn2"){
			var sf = $(this),
				par = sf.parent(),
				parTbl=sf.parents('table'),
				process = par.find('.inProcess'),
				complete = par.find('.complete'),
				allActionBtns = $('td.actionBtns').find('button').not(':.btnOff').addClass('btnOff'),
				allUpdBtns = $('button.updateBtn').addClass('btnOff');
				sf.hide('fast', function () {
					process.removeClass('hidden');
				});
			setTimeout(function () {
				process.addClass('hidden');
				if(this.id!="sdaBtn2" && this.id!="enrollBtn5"){
					if (sf.next().prop("tagName").toLowerCase() === 'button') {
						sf.next().removeClass('hidden');
					} else {
					   sf.show();
					};

					if(enrollDiscFlg && (vdoFlag1 || vdoFlag2) && (sf.attr("title")).toLowerCase()=="enroll"){
					    complete.removeClass('hidden');
					    process.remove();
					    sf.hide();
					}
					if (complete.length <= 0) {
						sf.show();
						sf.closest('tr').find('span.sdaDate').text(Date.today().toString('MM/dd/yyyy'));
					};
				}
				allActionBtns.removeClass('btnOff');
				allUpdBtns.removeClass('btnOff');
			}, 2000);
		//}

		
			switch(this.id){
				case "sdaBtn1":
				// case "sdaBtn2":
				// 	$.merge($sdaBtn1,$sdaBtn2).addClass('hidden');
				// 	$.merge($eBtn1,$eBtn2).removeClass('hidden');
					break;
				// case "modify-sda-1":
				// 	$('#startVideo').dialog('open');
				// 	loadFlash();
				// 	break;
				case "sdaBtn7": $('#startVideo').dialog('open');
								//$('.label').removeClass('closed');
								loadFlashMovie('assets/video/SAOSDA_Proxy_Video.swf');
								vdoFlag2 = true;
								$(this).addClass('hidden');
								break;
				//case 'enrollBtn2':
				case 'enrollBtn1':				
				case 'enrollBtn3':
					if(!enrollDiscFlg){
						$("#saveResidentialAdd").dialog('open');
						$(".addrFields").hide();
						$("#USA").show();
						$("#RA_Country1").on('change',function(){
							var country=$(this).val();
							$(".addrFields").hide();
							$('#'+country).show();
						});

						$("#res-add-alert,#res-add-alert li").addClass("hidden").removeClass("visible");
						$("#saveResidentialAdd .btn, #saveResidentialAdd .btn3").on('click',function(){
							var saveBtnCtr=0;
							if(this.id=="saveAddress"){ 
								$("#USA [aria-required=true]").each(function(){
									var resAddIp=$("#"+this.id),
										resAddIpVal=$.trim(resAddIp.val());
										resAddErr=$("#"+this.id+"-err");
									if(resAddIpVal==0 || typeof resAddIpVal == "undefined" || resAddIpVal =="none" || resAddIpVal=="" || resAddIpVal=="mm/dd/yyyy"){
										if(resAddErr.length>0){
											resAddErr.removeClass('hidden');
											saveBtnCtr=1;
										}
									}
									else{
										resAddErr.addClass('hidden');
									}
								});

								if(saveBtnCtr==1){
									$('#res-add-alert').removeClass('hidden').addClass("visible");
									saveBtnCtr=0;
								}
								else{
									$("#saveResidentialAdd").dialog("close");
									$("#res-add-sec").removeClass("hidden");
								}
								enrollDiscFlg=true;
							}
							else{
								$("#saveResidentialAdd").dialog("close");
							}
						});
					}
					break;
			}
		}

	});
	$('td.actionBtns').find('button').not('.btnOff').on('click', function () {
	 switch(this.id){
            case 'enrollBtn6':  
            					if(!enrollDiscFlg){
	            					$("#saveResidentialAdd").dialog('open');
	                                $(".addrFields").hide();
	                                $("#USA").show();
	                                $("#RA_Country1").on('change',function(){
	                                    var country=$(this).val();
	                                    $(".addrFields").hide();
	                                    $('#'+country).show();
	                                });
	                                $("#saveResidentialAdd .btn, #saveResidentialAdd .btn3").on('click',function(){
	                                    $("#saveResidentialAdd").dialog("close");
	                                });
									$('#enrollBtn6').addClass('hidden');
								}
                                break;
            case 'enrollBtn5':  
            					if(!enrollDiscFlg){
	            					$('#verification').dialog("open");
	                                 $("#verification .btn,#verification .btn3").on('click',function(){
	                                 	if(this.id=="saveAddress"){ 
	                                 	
	                                 	}
	                                    $("#verification").dialog("close");
	                                });
									$('#enrollBtn5').addClass('hidden');
                                }
                                break;
			case 'enrollBtn7':  enrollDiscFlg=true;
								$('#enrollBtn7').addClass('hidden');
                                break; 
            case 'enrollBtn11':$('#enrollBtn11').addClass('hidden');
                                break;
            case 'enrollBtn12': $('#enrollBtn12').addClass('hidden');
                                break;
        }

	
		
		
		
       var sf = $(this),
            par = sf.parent(),
            process = par.find('.inProcess'),
            complete = par.find('.complete'),
            allActionBtns = $('td.actionBtns').find('button').not(':.btnOff').addClass('btnOff'),
            allUpdBtns = $('button.updateBtn').addClass('btnOff');
            sf.hide('fast', function () {
                process.removeClass('hidden');
            });
        setTimeout(function () {
            process.addClass('hidden');
            if (sf.next().prop("tagName").toLowerCase() === 'button') {
                sf.next().removeClass('hidden');
            } else {
                complete.removeClass('hidden');
            };
            if (complete.length <= 0) {
                sf.show();
                sf.closest('tr').find('span.sdaDate').text(Date.today().toString('MM/dd/yyyy'));
            };
            allActionBtns.removeClass('btnOff');
            allUpdBtns.removeClass('btnOff');
        }, 2000);
    });
	
	/*Function to display s18 button functionality*/
	$('td.actionBtns').find('button').not('.btnOff').on('click', function () {
		// localStorage.btnChange = "1";
	  //  if($(this).hasClass('btn2')){
		//if(this.id!="sda1"){
			var sf = $(this),
				par = sf.parent(),
				parTbl=sf.parents('table'),
				process = par.find('.inProcess'),
				complete = par.find('.complete'),
				allActionBtns = $('td.actionBtns').find('button').not(':.btnOff').addClass('btnOff'),
				allUpdBtns = $('button.updateBtn').addClass('btnOff');
				sf.hide('fast', function () {
					process.removeClass('hidden');
				});
			 setTimeout(function () {
                process.addClass('hidden');
                if (sf.next().prop("tagName").toLowerCase() === 'button') {
                  sf.next().removeClass('hidden');
                } else {
                complete.addClass('hidden');
                };
               if (complete.length <= 0) {
                   sf.show();
                   sf.closest('tr').find('span.sdaDate').text(Date.today().toString('MM/dd/yyyy'));
                };
               allActionBtns.removeClass('btnOff');
              allUpdBtns.removeClass('btnOff');
			  
		    	if($('#modifySDA').hasClass('hidden') == false || $('#modifySDA1').hasClass('hidden') == false)
			    {	
				   $('.fastlaneBtn').removeClass('closed');
			    }else{
				 $('.fastlaneBtn').addClass('closed');
			    }
			    if($('#enrollBtnNF').hasClass('hidden') == false || $('#modifySDA2').hasClass('hidden') == false)
			    {	
				  $('.noFastLaneTIAABtn').removeClass('closed');										
			    }else{
				  $('.noFastLaneTIAABtn').addClass('closed');
			    }
				 if($('#modifySDA3').hasClass('hidden') == false)
			    {	
				  $('.noFastLaneExtBtn').removeClass('closed');										
			    }else{
				  $('.noFastLaneExtBtn').addClass('closed');
			    }
			  			
			 /* $($modifySda3).on('click', function(){
			   $('.noFastLaneExtBtn').removeClass('closed');         	
	         });*/
			 
            }, 2500);
			
		
		//}
		
		
		//}
	});
	
	$('#tooltip1').on('click', function(){
	$("#fastLaneHelp").dialog('open');
	
	}); 
$(function(){
  var universities =  ["Howard University","Howard College","Howard Middle School","Howard K-6","Ron Howard Middle School","Showdown School"]; 
   $( "#searchBox-input" ).autocomplete({source: universities});});  
 
	$('#accessSearch').on('click', function(e){
		if($('#searchBox-input').val() ==  $('#Select_Location1').val()){
			$("#tbl_accesscode tr.hiderow").removeClass('closed');
		}else{
			e.preventDefault();
		}	
	}); 
	$('#accessClear').on('click', function(e){
		$('#searchBox-input').val('');
		$('#Select_Location1').val('Select');
		$("#tbl_accesscode tr.hiderow").addClass('closed');
	}); 
   
	$('#disclosure-verification').on('change',function(){
		if ($(this).is(':checked')){
			$('#saveAddress').removeClass('btnOff');
		}
		else{
			$('#saveAddress').addClass('btnOff');
		}
	});

	$('#chkverification').on('change',function(){
		if ($(this).is(':checked')){
			$('#saveVerification').removeClass('btnOff');
		}
		else{
			$('#saveVerification').addClass('btnOff');
		}
	});
	
	$('.ssn').keyup(function(){
		if ($(this).attr('id')== 'ssn'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#ssn1').focus();
			}
		}
		if ($(this).attr('id')== 'ssn1'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#ssn2').focus();
			}
		}
	});
	
	$('.ssn2').keyup(function(){
		if ($(this).attr('id')== 'ssn2_1'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#ssn2_2').focus();
			}
		}
		if ($(this).attr('id')== 'ssn2_2'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#ssn2_3').focus();
			}
		}
	});

	
	
		$('.phone').keyup(function(){
		if ($(this).attr('id')== 'dayphone1'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#dayphone2').focus();
				
			}
		}
		if ($(this).attr('id')== 'dayphone2'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#dayphone3').focus();
				
			}
		}
	});
	$('.phone1').keyup(function(){
		if ($(this).attr('id')== 'evephone1'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#evephone2').focus();
				
			}
		}
		if ($(this).attr('id')== 'evephone2'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#evephone3').focus();
				
			}
		}
	});

	$('#accessCodeGo').on('click',function(){
		var aCode=$("#accessCode").val();

		if(aCode!=$("#accessCode1").val())
		if(aCode.length > 0 && /^[a-zA-Z0-9- ]*$/.test(aCode)) {
			$('#subAlert2').removeClass('visible');
			switch(aCode){
				case '101010a':
					$(".elg-plans").addClass("closed");
					$("#eplans-sec2a").removeClass("closed");
					break;
				case '405800':
					$(".elg-plans").addClass("closed");
					$("#eplans-sec2b").removeClass("closed");
					break;
				case '151212':
					$(".elg-plans").addClass("closed");
					$("#eplans-sec1").removeClass("closed");
					break;
				case '406081':
					$(".elg-plans").addClass("closed");
					$("#eplans-sec2").removeClass("closed");
					break;
				default: 
					$("#err-acc-code2").html($("#accessCode").val());
					$('#subAlert2').addClass('visible');
					break;
			}
		}
	})

	var flashMovie = null;

	function loadFlashMovie(movieUrl){
		flashMovie = $('#flashInteract .movie');
		flashMovie.flash({
		swf: movieUrl,
		width: 800,
		height: 600,
		play: true
		});
	};

	$("#startVideo").on("dialogclose", function(event, ui) {
		$('#flashInteract .movie').html('');
	});

	$.merge($sdaBtn1,$sdaBtn2).on('click', function(){
		//$('#startVideo').dialog('open');
		//loadFlashMovie('assets/video/Patricia_Pension_Cornell_Video.swf');
		vdoFlag1 = true;
	});

    $.merge($sdaBtn3,$sdaBtn4).on('click', function(){
		//$('#startVideo1').dialog('open');
		//$('.label').removeClass('closed');
		//loadFlashMovie('assets/video/Patricia_Pension_NYS_VDC_Video.swf');
		vdoFlag2 = true;
	});

	 // $("#modify-sda-1").on('click',function(){
	 // 	$('#startVideo').dialog('open');
		//loadFlashMovie('assets/video/Patricia_Pension_Cornell_Video.swf');
	 // });
	  $($sdaBtn7).on('click', function(){
		
	});
 $(".video1").on('click',function(){
	$('#startVideo').dialog('open');
	 loadFlashMovie('assets/video/OPE_SIMULATE_before_submitting_Enrollment.swf');
 });
  $(".sim2").on('click',function(){
	$('#startVideo').dialog('open');
	 loadFlashMovie('assets/video/SRKOLE_ELIG_NYU_2_Plans.swf');
 });
	//to set default date to 1997
	$('#dob').datepicker({
        changeMonth: true,
        changeYear: true,
		yearRange: '1900:2000',
        minDate: '01/01/1900',
        maxDate: '12/31/1997'
    });

	//Hide DatePicker on Mouse Wheel scroll 
    $("body").bind('DOMMouseScroll mousewheel', function(e){
         $('#dob').datepicker("hide").blur();
    });

});

