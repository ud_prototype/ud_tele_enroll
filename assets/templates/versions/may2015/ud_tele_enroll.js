$(document).on('bodyContLoaded', function() {
	// sprint-X - Scripts
	var existUser, srkole_enroll, retirement_enroll, saosda_enroll, ope_enroll, address,
		$participantForm    = $('#ParticipantDetails').next().find('.proto_new'),
		$participantDetails = $('#ParticipantDetails').next().find('.proto_existing'),
		$partcipantHeader   = $('#ParticipantDetails1'),
		$eligibleHeader     = $('#EligiblePlans1'),
		$activeHeader       = $('#ActivePlans1');
	
	existUser          	    = (store.get('users') === 'existUser') ? true : false;
	address          	    = (store.get('address') === 'true') ? true : false;
	srkole_enroll           = (store.get('enrollment') === 'srkole') ? true : false;
	retirement_enroll       = (store.get('enrollment') === 'retirement') ? true : false;
	saosda_enroll           = (store.get('enrollment') === 'saosda') ? true : false;
	ope_enroll              = (store.get('enrollment') === 'ope') ? true : false;
	
	$('.today').text(Date.today().toString('MM/dd/yyyy'));
	$(".lastfivedays").text(Date.today().addDays(-5).toString('MM/dd/yyyy'));
	$(".lastsevendays").text(Date.today().addDays(-7).toString('MM/dd/yyyy'));

	// SetUP Show and hide when page loads
	$('#lookupForm2, #lookupTable, #eplans-sec2a, #eplans-sec2b, #eplans-sec1, #eplans-secView').add($participantDetails).addClass('closed');
	$('.pageTitleUD').find('.proto_new').removeClass('closed');
	
	$('#elookup').on('click', function() {
		var ssn01 = $('#ssn').val(),
			ssn02 = $('#ssn1').val(),
			ssn03 = $('#ssn2').val();
		if (existUser) {
			$('.pageTitleUD').find('.proto_existing').removeClass('closed').end().find('.proto_new').addClass('closed');
			$('#EnrollmentLookUp1').find('a').trigger('click');
			$partcipantHeader.find('a').hasClass('collapsed') && $partcipantHeader.find('a').trigger('click');
			$eligibleHeader.find('a').hasClass('collapsed') && $eligibleHeader.find('a').trigger('click');
			$activeHeader.find('a').hasClass('collapsed') && $activeHeader.find('a').trigger('click');
			$participantForm.addClass('closed');
			$participantDetails.removeClass('closed');
			$("#eplans-sec1, #eplans-secView").removeClass("closed");
			$(".noDataEligTable,#eplans-sec2a").addClass("closed");
			$('#activePlanTbl .row1').removeClass("closed");
			$('.hideActiveTable').hide();
			$('.showActiveTable').show();
		};
		$('#ssn2_1').val((ssn01 == "" || ssn01 == null) ? "123" : ssn01);
		$('#ssn2_2').val((ssn02 == "" || ssn02 == null) ? "12" : ssn02);
		$('#ssn2_3').val((ssn03 == "" || ssn03 == null) ? "1234" : ssn03);
		$("#lblSSN").html(((ssn01 == "" || ssn01 == null) ? "123" : ssn01) + '-' + ((ssn02 == "" || ssn02 == null) ? "12" : ssn02) + '-' + ((ssn03 == "" || ssn03 == null) ? "1234" : ssn03));
		$(window).trigger('resize');
	});

	$('#lookupCancelBtn').on('click', function() {
		$('#lookupForm2').addClass('closed');
		$('#lookupForm1').removeClass('closed');
		$partcipantHeader.find('a').hasClass('expanded') && $partcipantHeader.find('a').trigger('click');
		$eligibleHeader.find('a').hasClass('expanded') && $eligibleHeader.find('a').trigger('click');
		$('#ActivePlans1').find('a').hasClass('expanded') && $('#ActivePlans1').find('a').trigger('click');
		$('#lookupTable').addClass('closed');
		$('#lookupTableNoData').removeClass('closed');
		$("#accessCode1").val('');
		$.merge($("#lookupNextBtn"), $("#searchAccess")).addClass("btnOff");
		$("#mainAlert").hide();
	});

	$('.elookup').on('keyup', function() {
		var name = $(this).attr('name');
		if ($(this).val() !== '') {
			$(".elookup").attr("disabled", "disabled");
			$('input[name$=' + name + ']').removeAttr("disabled");
			$(this).focus();
		} else {
			$(".elookup").removeAttr("disabled");
		}
	});

	$('#resetEnb').on('click', function() {
		$(".elookup").removeAttr("disabled");
	});
	
	/*========== Functionality for Enrollment starts ==========*/
	if(srkole_enroll || retirement_enroll || saosda_enroll || ope_enroll){

		var _enrollment            = store.get('enrollment'),
			$saveBtn               = $("#savedSubmitBtn"),
			$submitBtn             = $("#submitFnrollBtn"),
			$verifyChk             = $("#chkverifySaved"),
			$verifyChk1            = $("#chkverify"),
			$savedEnrollPopUp      = $('#savedEnrollment'),
			$submitEnrollPopUp     = $('#submitEnrollment'),
			$compEditPopUp         = $('#completeEdit'),
			$compEdit1PopUp        = $('#completeEdit1'),
			$compEdit2PopUp        = $('#completeEdit2'),
			$compEdit3PopUp        = $('#completeEdit3'),
			$cancelEnrollPopUp     = $('#cancelEnroll'),
			$enrollBtn             = $('#enrollBtn'),
			$enrollPencil          = $('#enrollPencil'),
			$savedBtn              = $('.savedBtn'),
			$sdaBtn                = $('.sdaBtn'),
			$completeTxt           = $('.comTxt'),
			$participantName       = $('.pageTitleUD').find('.proto_existing'),
			_savedLbl 			   = $('.modal_savedlable').show(),
			_pendLbl 			   = $('.modal_pendinglable').hide(),
			$resAddressDiv         = $('#resAddressDiv'),
			$res2AddressDiv        = $('#res2AddressDiv'),
			$resCountry			   = $('#resCountry'),
			$resState			   = $('#resState'),
			$resLine1			   = $('#resLine1'),
			$resCity			   = $('#resCity'),
			$resZip1			   = $('#resZip1'),
			$res2Country		   = $('#res2Country'),
			$res2State			   = $('#res2State'),
			$res2Line1			   = $('#res2Line1'),
			$res2City			   = $('#res2City'),
			$res2Zip1			   = $('#res2Zip1'),
			$errorAlert            = $('#errorAlert'),
			$errorAlert2           = $('#errorAlert2'),
			$resAddressChk         = $('#resAddressChk'),
			$res2AddressChk        = $('#res2AddressChk'),
			$residentialAddr       = $('#lblresidential_address');

		if(address){
			$residentialAddr.find('span').removeClass('closed');
		};
			
	  	$('.protoExperience').add($resAddressDiv).add($res2AddressDiv).hide();
	  	$('.'+_enrollment+'Base').show();
		
		var saveBtnC = 0;
		//Validation function
		function validEach(ind){
			var pDetailsIp      = $("#" + ind),
				pDetailsIpVal   = $.trim(pDetailsIp.val());
			if(pDetailsIpVal == 0 || typeof pDetailsIpVal == "undefined" || pDetailsIpVal == "none" || pDetailsIpVal == ""){
				pDetailsIp.closest('.lblFieldPair').addClass('alertHighlight');
				saveBtnC = 1;
			}else{
				pDetailsIp.closest('.lblFieldPair').removeClass('alertHighlight');
			}
		}

		//Function for dailog close and saved enrollment button enable
		$("#enrollBtn").on('click', function(e) {
			// if(srkole_enroll){
			// 	$sdaBtn.removeClass('hidden');
			// 	$enrollBtn.addClass('hidden');
			// }
			// if(ope_enroll || saosda_enroll || retirement_enroll){
				if(!address){
					$res2AddressDiv.show();
					$res2AddressChk.removeAttr('checked');
					$res2AddressDiv.find('input[type=text]').val('');
					$res2AddressDiv.find('.lblFieldPair').removeClass('alertHighlight');
					$res2Country.add($res2State).val('none');
				}
				$submitEnrollPopUp.dialog("open");
				$submitEnrollPopUp.closest('.modal').find('.ui-dialog-title').html('Submit Enrollment for '+ $participantName.html());
				$submitBtn.addClass('btnOff');
				$verifyChk1.removeAttr('checked');
			// }
		});
		
		//Function for chkverify Checked or not
		$("#chkverify").on('click', function(e) {
			if($(this).is(':checked') == false){
				$submitBtn.addClass('btnOff');
			}else{
				$submitBtn.removeClass('btnOff');
			}
		});
		
		//Function for submitFnrollBtn Click
		$("#submitFnrollBtn").on('click', function(e) {
			if($(this).hasClass('btnOff')){
				e.preventDefault();
			}else{
				if(!address){
					$("#res2AddressDiv [aria-required=true]").each(function() {
						validEach(this.id);	
					});
					if(saveBtnC == 1){
						$errorAlert2.addClass('visible');
						saveBtnC = 0;
						e.preventDefault();
					}else{
						$errorAlert2.removeClass('visible');
						enrollSubmitV();
					}
				}else{
					enrollSubmitV();
				}
			}
		});
		
		//Function for submitFnrollBtn
		function enrollSubmitV(){
			$submitEnrollPopUp.dialog("close");
			$enrollBtn.add($enrollPencil).addClass('hidden');
			setTimeout(function() {
				if(srkole_enroll){
					$sdaBtn.removeClass('hidden');
					$enrollBtn.addClass('hidden');
				}else{
					$completeTxt.removeClass('hidden');
					$savedBtn.add('.inProcess').addClass('hidden');
				}
			}, 100);
		}
		
		//Function for dailog saved enrollment open
		$("#savedEnrollBtn").on('click', function() {
			if(!address){
				$resAddressDiv.show();
				$errorAlert.removeClass('visible');
				$resAddressChk.removeAttr('checked');
				$resAddressDiv.find('input[type=text]').val('');
				$resAddressDiv.find('.lblFieldPair').removeClass('alertHighlight');
				$resCountry.add($resState).val('none');
			}
			$savedEnrollPopUp.dialog("open");
			$savedEnrollPopUp.closest('.modal').find('.ui-dialog-title').html('Submit Enrollment with Disclosure for '+ $participantName.html());
			$saveBtn.addClass('btnOff');
			$verifyChk.removeAttr('checked');
		});
		
		//Function for entersdaBtn click
		$("#entersdaBtn").on('click', function() {
			$completeTxt.removeClass('hidden');
			$sdaBtn.addClass('hidden');
		});
		
		//Function for dailog saved enrollment Pencil Icon open
		$("#savedPencil").on('click', function() {
			if(srkole_enroll || ope_enroll){
				$compEdit1PopUp.dialog("open");
			}
			if(retirement_enroll || saosda_enroll){
				$compEditPopUp.dialog("open");
			}
		});
		
		//Function for cancel saved Enroll dailog Pop up closed
		$("#selectCancel1").on('click', function(e) {
			e.preventDefault();
			$compEditPopUp.dialog("close");
			$savedBtn.add($completeTxt).addClass('hidden');
			$enrollBtn.add($enrollPencil).removeClass('hidden');
			if(srkole_enroll){
				$enrollPencil.addClass('hidden');
			}
		});
		
		//Function for cancel saved Enroll dailog Pop up closed
		$(".cancelEnrollBtn").on('click', function(e) {
			e.preventDefault();
			$savedEnrollPopUp.dialog("close");
			$savedBtn.removeClass('hidden');
		});
		
		//Function for cancel saved Enroll dailog Pop up closed
		$(".cancelReset").on('click', function(e) {
			e.preventDefault();
			$submitEnrollPopUp.dialog("close");
			$errorAlert2.removeClass('visible');
			$savedBtn.addClass('hidden');
		});
		
		//Function for chkverifySaved Checked or not
		$("#chkverifySaved").on('click', function(e) {
			if($(this).is(':checked') == false){
				$saveBtn.addClass('btnOff');
			}else{
				$saveBtn.removeClass('btnOff');
			}
		});
		
		//Function for dailog close and saved enrollment button enable
		$("#savedSubmitBtn").on('click', function(e) {
			if($(this).hasClass('btnOff')){
				e.preventDefault();
			}else{
				if(!address){
					$("#resAddressDiv [aria-required=true]").each(function() {
						validEach(this.id);	
					});
					if(saveBtnC == 1){
						$errorAlert.addClass('visible');
						saveBtnC = 0;
						e.preventDefault();
					}else{
						$errorAlert.removeClass('visible');
						enrollSubmit();
						$residentialAddr.html($resLine1.val()+'<br />'+$resCity.val()+', '+$resState.val()+' '+$resZip1.val()+'<br />'+$resCountry.val());
					}
				}else{
					enrollSubmit();
				}
			}
		});
		
		//Function for savedSubmitBtn click
		function enrollSubmit(){
			$savedEnrollPopUp.dialog("close");
			if(srkole_enroll){
				setTimeout(function() {
					$sdaBtn.removeClass('hidden');
					$savedBtn.addClass('hidden');
					$completeTxt.addClass('hidden');
				}, 500);
			}
			if(ope_enroll || saosda_enroll || retirement_enroll){
				setTimeout(function() {
					$completeTxt.removeClass('hidden');
					$savedBtn.addClass('hidden');
				}, 500);
			}
		}
		
		//Function for comPencil click
		$("#comPencil").on('click', function(e) {
			var _$obj;

			_savedLbl.hide();
			_pendLbl.show();

			if(ope_enroll){
				_$obj = $compEdit1PopUp;
			} else if(retirement_enroll || saosda_enroll || srkole_enroll){
				_$obj = $compEditPopUp;
			};

			_$obj.dialog("open").dialog({
					title: "Enrollment Edits - Plan Name"
				}).off('dialogbeforeclose').on('dialogbeforeclose', function(){
					_savedLbl.show();
					_pendLbl.hide();
				});;
		});

		
		//Function for enrollPencil click
		$("#enrollPencil").on('click', function(e) {
			$compEdit2PopUp.dialog("open");
		});
		
		//Function for sdaPencil click
		$("#sdaPencil").on('click', function(e) {
			if(srkole_enroll){
				$compEdit3PopUp.dialog("open");
			}
			if(ope_enroll || saosda_enroll){
				$compEdit1PopUp.dialog("open");
			}
		});
		
		//Function for selectCancel click
		$("#selectCancel").on('click', function(e) {
			e.preventDefault();
			$compEdit1PopUp.dialog("close");
			$cancelEnrollPopUp.dialog("open");
		});
		
		//Function for selectCancel2 click
		$("#selectCancel2").on('click', function(e) {
			e.preventDefault();
			$compEdit3PopUp.dialog("close");
			$enrollBtn.removeClass('hidden');
			$sdaBtn.addClass('hidden');
		});
		
		//Function for cancelYes click
		$("#cancelYes").on('click', function(e) {
			e.preventDefault();
			$cancelEnrollPopUp.dialog("close");
			$enrollBtn.removeClass('hidden');
			$completeTxt.add($savedBtn).addClass('hidden');
		});
		
		//Function for resAddressChk click
		$("#resAddressChk").on('click', function() {
			if($(this).is(':checked')){
				$resCountry.val('United States Of America');
				$resState.val('NY');
				$resLine1.val('26 THOMAS ST');
				$resCity.val('STATEN ISLAND');
				$resZip1.val('10306');
			}else{
				$resAddressDiv.find('input[type=text]').val('');
				$resCountry.add($resState).val('none');
			}
		});
		
		//Function for res2AddressChk click
		$("#res2AddressChk").on('click', function() {
			if($(this).is(':checked')){
				$res2Country.val('United States Of America');
				$res2State.val('NY');
				$res2Line1.val('26 THOMAS ST');
				$res2City.val('STATEN ISLAND');
				$res2Zip1.val('10306');
			}else{
				$res2AddressDiv.find('input[type=text]').val('');
				$res2Country.add($res2State).val('none');
			}
		});
	}

	/*========== Functionality for Enrollment ends ==========*/
	
	// ssn keyup functionality for enrollment lookup
	$('.ssn').keyup(function() {
		if ($(this).attr('id') == 'ssn') {
			if ($(this).val().length == $(this).attr("maxlength")) {
				$('#ssn1').focus();
			}
		}
		if ($(this).attr('id') == 'ssn1') {
			if ($(this).val().length == $(this).attr("maxlength")) {
				$('#ssn2').focus();
			}
		}
	});
	
	// code for sorting table ID tbl_accesscode
	if($('#tbl_accesscode').length > 0){
		var tbl_persons__js = new tiaacref.tableSort({
			id: '#tbl_accesscode',
			multiSort: false,
			onBeforeSort: function(e){''},
			onAfterSort: function(e){''},
			caseSensitive: false,
			initialSort: ''
		});
		tbl_persons__js.sort( 'accessCodetbl', 'desc', false );
	}

	$("#successCancel").on('click', function() {
		$("#elgbPlnsAdd11 .complete").addClass("hidden");
	});

	var _beam = urlData.getURLname();

	if(_beam.indexOf('beam') > 1){
	// Loading file for Atom Beam 
		$.getScript('assets/js/script.js', function(data){
			new tiaacref.menu({
	            id: "#mmmenu-plan_history",
	            style: "megamenu",
	            attachTo: "#menu-plan_history"
	        });

			var autoincreaseTooltippreview = new tiaacref.tooltip({
	            icon: 'tipLink',
	            id: '#tcId0',
	            size: 'large',
	            detached: false,
	            position: 'top',
	            attachTo: '#tooltip-ERmandatory_0_0_1427899719480',
	            tooltip: '#tooltiptcId0',
	            style: ''
	        });

	        var autoincreaseTooltippreview = new tiaacref.tooltip({
	            icon: 'tipLink',
	            id: '#tcId1',
	            size: 'large',
	            detached: false,
	            position: 'top',
	            attachTo: '#tooltip-mandatory_0_1_1427899719480',
	            tooltip: '#tooltiptcId1',
	            style: ''
	        });

		});
	};

});