

$(document).on('bodyContLoaded', function () {
	
	//sp8Scn='ssnNo';

	var enrollDiscFlg=false;

	(sp8Scn === 'ssnNo') ?  store.set('user', 'new'): store.set('user', 'exist');
	userInfo = store.get('user');

	$('.today').text(Date.today().toString('MM/dd/yyyy'));
	$(".lastfivedays").text(Date.today().addDays(-5).toString('MM/dd/yyyy'));
	$(".lastsevendays").text(Date.today().addDays(-7).toString('MM/dd/yyyy'));
	
	$('#ParticipantDetails1').find('a').attr('class', 'collapsed');
	$('#EligiblePlans1').find('a').attr('class', 'collapsed');
	$('#ActivePlans1').find('a').attr('class', 'collapsed');
	$('#lookupForm2, #lookupTable').addClass('closed');

	if (sp8Scn === 'ssnYes') {
		$('#EnrollmentLookUp1').find('a').attr('class', 'collapsed');
		// $('div.panels').find('.hd:eq(0)').find('a').trigger('click');
		$('#ParticipantDetails1').find('a').attr('class', 'expanded');
		$('#EligiblePlans1').find('a').attr('class', 'expanded');
		$('.proto_new').addClass('closed');
		$('.proto_existing').removeClass('closed');
		$('.noDataEligTable').addClass('closed')
		enrollDiscFlg=true;
		$("#res-add-sec").removeClass("hidden");

	} else {
		// $('#ParticipantDetails1').find('a').attr('class', 'collapsed');
		// $('#EligiblePlans1').find('a').attr('class', 'collapsed');
		// $('#ActivePlans1').find('a').attr('class', 'collapsed');
		$('.proto_new').removeClass('closed');
		$('.proto_existing').addClass('closed');
		$('.dataEligTable').addClass('closed');
	};

	// $('.pageTitleUD').find('.proto_new').removeClass('closed');
	// $('.pageTitleUD').find('.proto_existing').addClass('closed');


	$('#elookup').on('click', function(){
		var ssn01 = $('#ssn').val(),
			ssn02 = $('#ssn1').val(),
			ssn03 = $('#ssn2').val();
		if (sp8Scn === 'ssnNo') {
			$('#lookupForm1').addClass('closed');
			$('#lookupForm2').removeClass('closed');
			$('#mainAlert').slideDown();
		}else{
			$('#EnrollmentLookUp1').find('a').trigger('click');
			$('#ParticipantDetails1').find('a').trigger('click');
			$('#EligiblePlans1').find('a').trigger('click');
			$('#ActivePlans1').find('a').trigger('click');
			$('.pageTitleUD').find('.proto_new').addClass('closed');
			$('.pageTitleUD').find('.proto_existing').removeClass('closed');
		};
		$('#ssn2_1').val((ssn01==""||ssn01==null)?"123":ssn01);
		$('#ssn2_2').val((ssn02==""||ssn02==null)?"12":ssn02);
		$('#ssn2_3').val((ssn03==""||ssn03==null)?"1234":ssn03);
	});

	var addFlag = false,
		saveBtnCtr=0;
			
			
	$('#saveBtn').on('click', function () {
	var dayphoneno = "(" + $('#dayphone1').val() + ")" + " " +  $('#dayphone2').val() + " - " + $('#dayphone3').val();
	var evephoneno = "(" + $('#evephone1').val() + ")" + " " +  $('#evephone2').val() + " - " + $('#evephone3').val();
			var ssnno = $('#ssn2_1').val() + "-" +  $('#ssn2_2').val() + "-" + $('#ssn2_3').val();
			var dob = $('#dob').val();
			var email = $('#email').val();
			var gender = $('#Gender').val();
			var empid = $('#empid').val();
			var mailAdd= $('#ma_line1').val()+[($('#ma_line2').val()!="")?'<br/>'+$('#ma_line2').val():'']+[($('#ma_line3').val()!="")?'<br/>'+$('#ma_line3').val():'']; 
			var mailState = $('#ma_city').val()+', '+$('#ma_state').val();
			var mailCountry = $('#MA_Country').val();
			var zipcode = $('#ma_zip_1').val();
	$("#Pdalerts,#Pdalerts li").addClass("hidden").removeClass("visible");
	$("#defaultform [aria-required=true]").each(function(){
				var pDetailsIp=$("#"+this.id),
					pDetailsIpVal=$.trim(pDetailsIp.val());
					pDetailsErr=$("#"+this.id+"-err");
				if(pDetailsIpVal==0 || typeof pDetailsIpVal == "undefined" || pDetailsIpVal =="none" || pDetailsIpVal=="" || pDetailsIpVal=="mm/dd/yyyy"){
					if(pDetailsErr.length>0){
						pDetailsErr.removeClass('hidden');
						saveBtnCtr=1;
					}
				}
				else{
					pDetailsErr.addClass('hidden');
				}
			});

		if(saveBtnCtr==1){
			$('#Pdalerts').removeClass('hidden').addClass("visible");
			saveBtnCtr=0;
		}
		else if(!addFlag && saveBtnCtr==0){
			$("#Pdalerts,#Pdalerts li").addClass("hidden").removeClass("visible");
			
			$("#wrapper").find('#Pdalerts').addClass('hidden').removeClass('visible');
			$("#mAddress,#uspsAddress").html(mailAdd + "<br/>" + mailState + " " + zipcode+"<br/>"+mailCountry);
			$("#saveFormData").dialog('open');
			addFlag = true;
			$('.saveadd').on ('click',function(){
				$("#saveFormData").dialog('close');
				$('#EnrollmentLookUp1').find('a').hasClass('expanded') && $('#EnrollmentLookUp1').find('a').trigger('click');
				$('#EligiblePlans1').find('a').hasClass('collapsed') && $('#EligiblePlans1').find('a').trigger('click');
				//$('#ActivePlans1').find('a').hasClass('collapsed') && $('#ActivePlans1').find('a').trigger('click');
				$('.proto_new').addClass('closed');
				$('#lblSSN').html(ssnno);
				$('#lblmailing_address').html(mailAdd + "<br/>" + mailState + " " + zipcode+"<br/>"+mailCountry);
				$('#lbldaytime_phone').html(dayphoneno);
				$('#lblevening_phone').html(evephoneno);
				$('#lblDOB').html(dob);
				$('#lblEmail').html(email);
				$('#lblgender').html(gender);
                $('#lblempid').html(empid);	
                $('#lblresidential_address').html(mailAdd + "<br/>" + mailState + " " + zipcode+"<br/>"+mailCountry);								
				$('.proto_existing').removeClass('closed');
				$('.dataEligTable').removeClass('closed');
				$('.noDataEligTable').addClass('closed');
			
		});
		}else if(!saveBtnCtr){
		
		};
	});

		$("#accessCode1").on('keyup blur',function(){   
			var aCodeObj=$("#searchAccess");
			($(this).val().length>0)?aCodeObj.removeClass("btnOff"):aCodeObj.addClass("btnOff");
		})

		var clientName = $('#clientName').html();
		$('#clientName').html('');

		$('#searchAccess').on('click', function(){var newTimer, newTimer1, newTimer2, thisVal = $('#accessCode1').val();
			// clearTimeout(newTimer); 
			// clearTimeout(newTimer2);
			// $('#lookupBtns').find('input:eq(0)').addClass('btnOff');
			// $('#clientName').html('');
			// $('#lookupTable').addClass('closed');
			// $('#lookupTableNoData').removeClass('closed'); 
			clearTimeout(newTimer1);  
			// newTimer = setTimeout(function(){
					if(thisVal.length > 0 && /^[a-zA-Z0-9- ]*$/.test(thisVal)){
						$('#lookupProcess').css('visibility', 'visible');
						clearTimeout(newTimer1);
						newTimer1 = setTimeout(function(){
							 $('#lookupTableNoData').addClass('closed');
							 $('#lookupTable').removeClass('closed');
							 $('#lookupBtns').find('input').removeClass('btnOff');
							 $('#lookupProcess').css('visibility', 'hidden');
							 $('#clientName').html(clientName);
							 $('#subAlert1').hide()
						}, 1000);
					}else{
						$('#subAlert1').show();
						$('#lookupBtns').find('input:eq(0)').addClass('btnOff');
			$('#clientName').html('');
			$('#lookupTable').addClass('closed');
			$('#lookupTableNoData').removeClass('closed'); 
					};
				// }, 1000);
				return false;

		});


		$('#lookupNextBtn').on('click', function(){
			if(userInfo === 'new'){
				//$('#EnrollmentLookUp1').find('a').hasClass('expanded') && $('#EnrollmentLookUp1').find('a').trigger('click');
				$('#ParticipantDetails1').find('a').hasClass('collapsed') && $('#ParticipantDetails1').find('a').trigger('click');
				$('#EligiblePlans1').find('a').hasClass('expanded') && $('#EligiblePlans1').find('a').trigger('click');
				$('#ActivePlans1').find('a').hasClass('expanded') && $('#ActivePlans1').find('a').trigger('click');
				$("#mainAlert").hide();
			};
		});

		$('#lookupCancelBtn').on('click', function(){
			$('#lookupForm2').addClass('closed');
			$('#lookupForm1').removeClass('closed');
			$('#ParticipantDetails1').find('a').hasClass('expanded') && $('#ParticipantDetails1').find('a').trigger('click');
			$('#EligiblePlans1').find('a').hasClass('expanded') && $('#EligiblePlans1').find('a').trigger('click');
			$('#ActivePlans1').find('a').hasClass('expanded') && $('#ActivePlans1').find('a').trigger('click');

			$('#lookupTable').addClass('closed');
			$('#lookupTableNoData').removeClass('closed'); 
			$("#accessCode1").val('');
			$.merge($("#lookupNextBtn"),$("#searchAccess")).addClass("btnOff");
			$("#mainAlert").hide();
		});    





	// $('td.actionBtns').find('button').not('.btnOff').on('click', function () {
	//     localStorage.btnChange = "1";
	//     var sf = $(this),
	//         par = sf.parent(),
	//         process = par.find('.inProcess'),
	//         complete = par.find('.complete'),
	//         allActionBtns = $('td.actionBtns').find('button').not(':.btnOff').addClass('btnOff'),
	//         allUpdBtns = $('button.updateBtn').addClass('btnOff');
	//     sf.hide('fast', function () {
	//         process.removeClass('hidden');
	//     });
	//     setTimeout(function () {
	//         process.addClass('hidden');
	//         if (sf.next().prop("tagName").toLowerCase() === 'button') {
	//             sf.next().removeClass('hidden');
	//         } else {
	//             complete.removeClass('hidden');
	//         };
	//         if (complete.length <= 0) {
	//             sf.show();
	//             sf.closest('tr').find('span.sdaDate').text(Date.today().toString('MM/dd/yyyy'));
	//         };
	//         allActionBtns.removeClass('btnOff');
	//         allUpdBtns.removeClass('btnOff');
	//     }, 2000);
	// });



	 var $eBtn1 = $('#enrollBtn1'),
		$eBtn2 = $('#enrollBtn2'),
		$eBtn3 = $('#enrollBtn3'),
		$eBtn4 = $('#enrollBtn4'),
		$sdaBtn1 = $('#sdaBtn1'),
		$sdaBtn2 = $('#sdaBtn2'),
		$sdaBtn3 = $('#sdaBtn3'),
		$sdaBtn4 = $('#sdaBtn4'),
		$uptBtn1 = $('#uptBtn1'),
		$uptBtn2 = $('#uptBtn2'),
		$toShow = $('.showActiveTable'),
		$toHide = $('.hideActiveTable'),
		$modifySda1 = $("#modify-sda-1"),
		vdoFlag1 = false,
		vdoFlag2 = false;

		$toShow.hide();

	//  $uptBtn1.on('click', function(){
	//     if(vdoFlag1 === true){
	//         $.merge($sdaBtn1,$eBtn2).hide();
	//         $.merge($sdaBtn1.parent(), $eBtn2.parent()).find('span.inProcess').removeClass('hidden');
	//         if($eBtn2.parent().find('span.complete').is(':visible')){
	//             $eBtn2.parent().find('span.inProcess').addClass('hidden');
	//         };
	//         setTimeout(function(){
	//             $.merge($sdaBtn1.parent(), $eBtn2.parent()).find('span.inProcess').addClass('hidden');
	//             $.merge($sdaBtn1.parent(), $eBtn2.parent()).find('span.complete').removeClass('hidden');
	//         }, 2000);
	//     }
	// });

	 $uptBtn1.on('click', function(){
		if(vdoFlag1 === true && enrollDiscFlg){
			$.merge($sdaBtn1,$sdaBtn2).hide();
			$.merge($eBtn1,$eBtn2).hide();
			$.merge($sdaBtn1.parent(), $sdaBtn2.parent()).find('span.inProcess').removeClass('hidden');
			setTimeout(function(){
				$toHide.hide();
				$('#ActivePlans1').find('a').hasClass('collapsed') && $('#ActivePlans1').find('a').trigger('click');	
				$('#elgbPlnsAdd .hideRow').addClass('closed');
				$('#elgbPlnsAdd .complete').removeClass('hidden');
				$.merge($sdaBtn1.parent(), $sdaBtn2.parent()).find('span.inProcess').addClass('hidden').remove();
				$eBtn2.removeClass('hidden');
				$toShow.show();
				$('#activePlanTbl .row1').removeClass("closed");
				// $.merge($sdaBtn3.parent(), $eBtn3.parent()).find('span.complete').removeClass('hidden');
			}, 2000);
		};
	});

	 $uptBtn2.on('click', function(){
		if(vdoFlag2 == true && enrollDiscFlg){
			$.merge($sdaBtn3,$sdaBtn4).hide();
			$.merge($eBtn3,$eBtn4).hide();
			$.merge($sdaBtn3.parent(), $sdaBtn4.parent()).find('span.inProcess').removeClass('hidden');
			setTimeout(function(){
				//$toHide.hide();
				//$('#ActivePlans1').find('a').hasClass('collapsed') && $('#ActivePlans1').find('a').trigger('click');	
				//$('#elgbPlnsAdd .hideRow').removeClass('closed');
				$('#aCode').addClass("hidden");
				$('#accessCode').val('');
				$('#eplans-sec3').removeClass("closed");
				$("#elgbPlns .complete").removeClass("hidden");
				$("#elgbPlnsAdd1 .complete").removeClass("hidden");
				$("#elgbPlnsAdd1 .hideRow").addClass("closed");
				$.merge($sdaBtn3.parent(), $sdaBtn4.parent()).find('span.inProcess').addClass('hidden').remove();
				//$eBtn4.removeClass('hidden');
				//$toShow.show();
				//$('#activePlanTbl .row2,#elgbPlns .no-data').removeClass("closed");
			}, 2000);
		};
	});
	
	$('.elookup').on('keyup', function () {
		var name = $(this).attr('name');
		if ($(this).val() !== '') {
			$(".elookup").attr("disabled", "disabled");
			$('input[name$=' + name + ']').removeAttr("disabled");
			$(this).focus();
		} else {
			$(".elookup").removeAttr("disabled");
		}
	});
	$('#resetEnb').on('click', function () {
		$(".elookup").removeAttr("disabled");
	});
	
	// $('#elookup').on('click', function () {
	//     window.location.href = 'tele_enroll.html?user=exist';
	// });
	// $('#editInfo').on('click', function () {
	//     window.location.href = 'tele_enroll.html?user=new';
	// });
	$('#sameaddress').on('click', function () {
		$pd = [{
			"ssn2_1": "123",
			"ssn2_2": "456",
			"ssn2_3": "7890",
			"f_name": "John",
			"l_name": "Smith",
			"email": "abcd@tiaa-cref.org",
			"dayphone1": "088",
			"dayphone2": "623",
			"dayphone3": "9786",
			"dob": "06/25/1954",
			"ma_line1": $("#ma_line1").val(),
			"ma_city": $("#ma_city").val(),
			"ma_zip_1": $("#ma_zip_1").val(),
			"ma_zip_2": $("#ma_zip_2").val(),
			"ma_state": $("#ma_state").val()
		}];

		$.each($pd, function (index, obj) {
			$("#ra_line1").val(obj.ma_line1);
			$("#ra_city").val(obj.ma_city);
			$("#ra_zip_1").val(obj.ma_zip_1);
			$("#ra_zip_2").val(obj.ma_zip_2);
			$("#ra_state option[value='"+obj.ma_state+"']").attr('selected', 'selected');
		});
	});

	$("#saveFormData").find('.btnBar button').on('click', function () {
		$("#saveFormData").dialog("close");
	});
	var popFlag = false;
	$('.popBtn').on('click', function(){
		popFlag = true;
	});


	$('td.actionBtns').find('button').not('.btnOff').on('click', function () {
		// localStorage.btnChange = "1";
	  //  if($(this).hasClass('btn2')){
		if(this.id!="sdaBtn2"){
			var sf = $(this),
				par = sf.parent(),
				parTbl=sf.parents('table'),
				process = par.find('.inProcess'),
				complete = par.find('.complete'),
				allActionBtns = $('td.actionBtns').find('button').not(':.btnOff').addClass('btnOff'),
				allUpdBtns = $('button.updateBtn').addClass('btnOff');
				sf.hide('fast', function () {
					process.removeClass('hidden');
				});
			setTimeout(function () {
				process.addClass('hidden');
				if(this.id!="sdaBtn2"){
					if (sf.next().prop("tagName").toLowerCase() === 'button') {
						sf.next().removeClass('hidden');
					} else {
					   sf.show();
					};

					if(enrollDiscFlg && (vdoFlag1 || vdoFlag2) && (sf.attr("title")).toLowerCase()=="enroll"){
					    complete.removeClass('hidden');
					    process.remove();
					    sf.hide();
					}
					if (complete.length <= 0) {
						sf.show();
						sf.closest('tr').find('span.sdaDate').text(Date.today().toString('MM/dd/yyyy'));
					};
				}
				allActionBtns.removeClass('btnOff');
				allUpdBtns.removeClass('btnOff');
			}, 2000);
		//}

		
			switch(this.id){
				case "sdaBtn1":
				// case "sdaBtn2":
				// 	$.merge($sdaBtn1,$sdaBtn2).addClass('hidden');
				// 	$.merge($eBtn1,$eBtn2).removeClass('hidden');
					break;
				// case "modify-sda-1":
				// 	$('#startVideo').dialog('open');
				// 	loadFlash();
				// 	break;

				//case 'enrollBtn2':
				case 'enrollBtn1':
				case 'enrollBtn3':
					if(!enrollDiscFlg){
						$("#saveResidentialAdd").dialog('open');
						$(".addrFields").hide();
						$("#USA").show();
						$("#RA_Country1").on('change',function(){
							var country=$(this).val();
							$(".addrFields").hide();
							$('#'+country).show();
						});

						$("#res-add-alert,#res-add-alert li").addClass("hidden").removeClass("visible");
						$("#saveResidentialAdd .btn, #saveResidentialAdd .btn3").on('click',function(){
							var saveBtnCtr=0;
							if(this.id=="saveAddress"){ 
								$("#USA [aria-required=true]").each(function(){
									var resAddIp=$("#"+this.id),
										resAddIpVal=$.trim(resAddIp.val());
										resAddErr=$("#"+this.id+"-err");
									if(resAddIpVal==0 || typeof resAddIpVal == "undefined" || resAddIpVal =="none" || resAddIpVal=="" || resAddIpVal=="mm/dd/yyyy"){
										if(resAddErr.length>0){
											resAddErr.removeClass('hidden');
											saveBtnCtr=1;
										}
									}
									else{
										resAddErr.addClass('hidden');
									}
								});

								if(saveBtnCtr==1){
									$('#res-add-alert').removeClass('hidden').addClass("visible");
									saveBtnCtr=0;
								}
								else{
									$("#saveResidentialAdd").dialog("close");
									$("#res-add-sec").removeClass("hidden");
								}
								enrollDiscFlg=true;
							}
							else{
								$("#saveResidentialAdd").dialog("close");
							}
						});
					}
	                
	    //             if(this.id=="enrollBtn2" && enrollDiscFlg){
		   //              $('#elgbPlnsAdd .hideRow').addClass('closed');
		 		// 		$('#activePlanTbl .row1').removeClass("closed");
		 		// 		$toHide.hide();
					// 	$('#ActivePlans1').find('a').hasClass('collapsed') && $('#ActivePlans1').find('a').trigger('click');
					// 	$toShow.show();
					// }
					break;
				
				// case 'enrollBtn4':  
				// case 'enrollBtn2':
				// 	$('#verification').dialog("open");
				// 	 $("#verification .btn,#verification .btn3").on('click',function(){
				// 		$("#verification").dialog("close");
				// 	});
				// 	break;    
			}
		}

	});

	$('#disclosure-verification').on('change',function(){
		if ($(this).is(':checked')){
			$('#saveAddress').removeClass('btnOff');
		}
		else{
			$('#saveAddress').addClass('btnOff');
		}
	});

	$('#chkverification').on('change',function(){
		if ($(this).is(':checked')){
			$('#saveVerification').removeClass('btnOff');
		}
		else{
			$('#saveVerification').addClass('btnOff');
		}
	});
	
	$('.ssn').keyup(function(){
		if ($(this).attr('id')== 'ssn'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#ssn1').focus();
			}
		}
		if ($(this).attr('id')== 'ssn1'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#ssn2').focus();
			}
		}
	});
	
			$('.ssn2').keyup(function(){
		if ($(this).attr('id')== 'ssn2_1'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#ssn2_2').focus();
			}
		}
		if ($(this).attr('id')== 'ssn2_2'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#ssn2_3').focus();
			}
		}
	});

	
	
		$('.phone').keyup(function(){
		if ($(this).attr('id')== 'dayphone1'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#dayphone2').focus();
				
			}
		}
		if ($(this).attr('id')== 'dayphone2'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#dayphone3').focus();
				
			}
		}
	});
	$('.phone1').keyup(function(){
		if ($(this).attr('id')== 'evephone1'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#evephone2').focus();
				
			}
		}
		if ($(this).attr('id')== 'evephone2'){
			if($(this).val().length==$(this).attr("maxlength")){
				$('#evephone3').focus();
				
			}
		}
	});

	$('#accessCodeGo').on('click',function(){
		var aCode=$("#accessCode").val();
		if(aCode.length > 0 && /^[a-zA-Z0-9- ]*$/.test(aCode)){
			$('#subAlert2').removeClass('visible');
			$("#eplans-sec2").removeClass("closed");
			$("#eplans-sec1").addClass("closed");
		}
		else{
			$('#subAlert2').addClass('visible');
		}
	})

	var flashMovie = null, flashMovie1 = null;
	function loadFlash(){
		flashMovie = $('#flashInteract .movie');
		flashMovie.flash({
		swf: 'assets/video/Patricia_Pension_Cornell_Video.swf',
		width: 800,
		height: 600,
		play: true
		});
	};
	function loadFlash1(){
		flashMovie1 = $('#flashInteract1 .movie');
		flashMovie1.flash({
		swf: 'assets/video/Patricia_Pension_NYS_VDC_Video.swf',
		width: 800,
		height: 600,
		play: true
		});
	};

	$("#startVideo").on("dialogclose", function(event, ui) {
		$('#flashInteract .movie').html('');
	});

	$("#startVideo1").on("dialogclose", function(event, ui) {
		$('#flashInteract1 .movie').html('');
	});

	$.merge($sdaBtn1,$sdaBtn2).on('click', function(){
		//$('#startVideo').dialog('open');
		//loadFlash();
		vdoFlag1 = true;
	});

    $.merge($sdaBtn3,$sdaBtn4).on('click', function(){
		//$('#startVideo1').dialog('open');
		//$('.label').removeClass('closed');
		//loadFlash1();
		vdoFlag2 = true;
	});

	 // $("#modify-sda-1").on('click',function(){
	 // 	$('#startVideo').dialog('open');
		// loadFlash();
	 // });

	//to set default date to 1997
	$('#dob').datepicker({
        changeMonth: true,
        changeYear: true,
		yearRange: '1900:2000',
        minDate: '01/01/1900',
        maxDate: '12/31/1997'
    });

	//Hide DatePicker on Mouse Wheel scroll 
    $("body").bind('DOMMouseScroll mousewheel', function(e){
         $('#dob').datepicker("hide").blur();
    });

});

