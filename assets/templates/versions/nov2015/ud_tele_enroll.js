$(document).on('bodyContLoaded', function() {

	/*============ Dec 2015  - Scripts : starts =============*/
	if(typeof store.get('users') === 'undefined'){
		store.set('users','existUser');	
	}
	
	if(typeof store.get('suny_rpe') === 'undefined'){	
		store.set('suny_rpe','rpeYes');	
	}	

	var existUser, noUser, newUser, address_descrepency,rpeYes, rpeNo,
		$participantForm    = $('#ParticipantDetails').next().find('.proto_new'),
		$participantDetails = $('#ParticipantDetails').next().find('.proto_existing'),
		$partcipantHeader   = $('#ParticipantDetails1'),
		$eligibleHeader     = $('#EligiblePlans1'),
		$openEnrollment     = $('#openEnrollment1'),
		$activeHeader       = $('#ActivePlans1'),
		$lookupForm1        = $('#lookupForm1'),
		$lookupForm2        = $('#lookupForm2'),
		$pageTitleUD		= $('.pageTitleUD'),
		$mainAlert			= $('#mainAlert'),
		$subAlert1          = $('#subAlert1'),
		$proto_rpeform 		= $('.proto_rpeform'),
		$eligiblePlansRpeTbl= $('#eligiblePlansRpeTbl'),
		$eligiblePlansTable = $('#eligiblePlansTable'),
		$rpeActivePlanTbl	= $('#rpeActivePlanTbl'),
		$allPanels 			= $('#ParticipantDetails,#openEnrollment, #EligiblePlans, #ActivePlans, #lookupTableNoData'),
		$showTitle			= $('.showTitle'),
		$accessCode1		= $("#accessCode1"),
		$lookupProcess		= $('#lookupProcess'),
		$lookupNextBtn 		= $('#lookupNextBtn');
		ie7Flag 			= ($.browser.msie && $.browser.version.substr(0, 1) < 8) ? true : false;
	
	existUser          	    = (store.get('users') === 'existUser') ? true : false;
	noUser          	    = (store.get('users') === 'noUser') ? true : false;
	newUser          	    = (store.get('users') === 'newUser') ? true : false;		
	rpeYes 					= (store.get('suny_rpe') === 'rpeYes') ? true:false;
	rpeNo  					= (store.get('suny_rpe') === 'rpeNo') ? true:false;
	
	
	if( (existUser && rpeYes) || (noUser && rpeYes) ){
		$proto_rpeform.removeClass('closed');	
		$('#openEnrollment').addClass('closed');
		if(ie7Flag){			
			$('#leftNav ul li').find('a').each(function(){
				if($(this).attr('title') === 'Open Enrollment'){					
					$('a[href="'+$(this).attr('href')+'"]').closest('li').addClass('closed');					
				}
			});
		}else{
			$('a[href=#openEnrollment1]').closest('li').addClass('closed');		
		}
	}else {
		$('.proto_existing').removeClass('closed');		
	}	
	if((noUser && rpeNo) || (newUser && rpeYes) || (newUser && rpeNo) ){
		$proto_rpeform.add('.titleContext').addClass('closed');	
		$('.showActiveTable').find('.proto_new').removeClass('closed');
	}
	/*============ Dec 2015  - Scripts : ends  =============*/
	
	
	/*============ Nov 2015  - Scripts : starts =============*/
	
	if(typeof store.get('details') === 'undefined') { store.set('details','srkNonDcaDetails'); }
	
	//View_details.html page scenario [SRK and MVC] functionality
	var $changeRS =  $('.changeRS'),
		srkNonDca =  (store.get('details') === 'srkNonDcaDetails' ) ? true : false,
		srkDca 	  =  (store.get('details') === 'srkDcaDetails' ) ? true : false,
		mvc       =  (store.get('details') === 'mvcDetails' ) ? true : false;
		
	if (srkDca){ 
		$('.proto_srk').removeClass('closed');		
	}
	if (srkNonDca){ 
		$('.proto_srkNon').removeClass('closed');		
	}
	if(mvc){  
		$('.proto_mvc').removeClass('closed');		
	}	
	
	/*============ Nov 2015  - Scripts : ends  =============*/
	
	
	/*============ Aug 2015  - Scripts =============*/
	
	
	$('.today').text(Date.today().toString('MM/dd/yyyy'));
	$(".lastfivedays").text(Date.today().addDays(-5).toString('MM/dd/yyyy'));
	$(".lastsevendays").text(Date.today().addDays(-7).toString('MM/dd/yyyy'));
	
	$('#lookupForm2, #lookupTable, #eplans-sec2a, #eplans-sec2b, #eplans-sec1, #eplans-secView').add($participantDetails).addClass('closed');	
	
	var ssn01 	   = $('#ssn'),
		ssn02 	   = $('#ssn1'),
		ssn03 	   = $('#ssn2'),
		npin  	   = $('#pin_npin');
		tNum  	   = $('#tiaa_cref_number'),
		numericReg = /^\d*$/;
		
	/*=========== Look Up Button Functionality starts here  ================*/
	
	//this function is common for Exist User- RPE Yes & Exist User - RPE No
	function rpeExistUsr(){
		var ssnVal     = ssn01.val()+ ssn02.val()+ ssn03.val(),
			npinVal    = npin.val(),
			tNumVal    = tNum.val();
		// SSN not empty and NPIN & TIAA Number is empty	
		if(ssnVal.length > 0 && ( npinVal.length === 0 && tNumVal.length === 0  ) ){
			if( ssn01.val().length != 3 || ssn02.val().length != 2 || ssn03.val().length != 4){
				invalidInput();	
			}else if( !numericReg.test(ssnVal) ){
				invalidInput();							
			}else if ( ssn01.val() == '' ||  ssn02.val() == '' || ssn03.val() == ''){
				invalidInput();				
			}else{						
				commonFumc2ExistUser();					
			}				
		}else if (npinVal.length > 0 && ( ssnVal.length === 0 && tNumVal.length === 0)) {   // NPIN not empty and SSN & TIAA Number is empty
			if( !numericReg.test(npinVal)) {
				invalidInput();											
			}else{						
				commonFumc2ExistUser();					
			}
		
		}else if(tNumVal.length > 0  && ( ssnVal.length === 0 && npinVal.length === 0 )) {  // TIAA Number not empty and SSN & NPIN is empty
			if( !numericReg.test(tNumVal) ){
				invalidInput();											  
			}else{						
				commonFumc2ExistUser();		
			}
		
		}else if (ssnVal.length === 0 && npinVal.length === 0 && tNumVal.length === 0){  // SSN , NPIN and TIAA Number all Empty
			invalidInput();	
		}
		
	}
	function rpeNoUser(){
		var msg, inputVal;	
		msg = ssn01.attr('data-error');
		$mainAlert.addClass('visible');		
		if(noUser && rpeNo){						
			if(ssn01.val() != '' || ssn02.val() != '' || ssn03.val() != ''){
				msg = ssn01.attr('data-error');				
			}else if(npin.val() != ''){			
				msg = npin.attr('data-error');
				inputVal = npin.val();			
			}else if(tNum.val()!= ''){			
				msg = tNum.attr('data-error');
				inputVal = tNum.val();			
			}
			$('span[data-alert='+msg+']').removeClass('closed').siblings().addClass('closed');
			$('span[data-alert='+msg+']').find('.pin').html(inputVal);
			$pageTitleUD.find('.proto_existing').addClass('closed').siblings('.proto_new').add('.unknowParticipant').removeClass('closed');
		}else {			
			$('span[data-alert='+msg+']').removeClass('closed').siblings().addClass('closed');
			
		}	
		
		$lookupForm1.addClass('closed');		
		$lookupForm2.removeClass('closed');	
		$subAlert1.hide();
		$('#lookupBtns').find('input:eq(0)').addClass('btnOff');
		$('#clientName,#clientName1,#clientName2').html('');
		$('#lookupTable,#lookupTable1').addClass('closed');
		$('#lookupTableNoData').removeClass('closed');
		$eligiblePlansTable.find('tbody tr').not('.noDataRow').addClass('closed');
		$lookupProcess.css('visibility', 'hidden');
		$('#lookupTableNoData,#lookupTable1,#lookupTable2,#lookupTable3,#lookupClientName').addClass('closed');
	}
	
	
	$('#elookup').on('click', function() {	
			if (existUser && rpeNo) {				
				rpeExistUsr();
				$eligiblePlansTable.find('tbody tr').removeClass('closed');
				$eligiblePlansTable.find('.noDataRow').addClass('closed');
				$pageTitleUD.find('.participantName').addClass('closed');		
				
			}else if (existUser && rpeYes){
					
				rpeExistUsr();	
				$eligiblePlansRpeTbl.find('tbody tr').not('.dataRow406081').removeClass('closed hidden')
				$rpeActivePlanTbl.find('tbody tr').add('.rpeshowTitle').removeClass('closed hidden');				
				$eligiblePlansRpeTbl.add($rpeActivePlanTbl).find('.noDataRow').addClass('closed');	
				if(! $('#eligiblePlansRpeTbl, #rpeActivePlanTbl').find('a.jsExpandCollapse').hasClass('expanded')){
					$eligiblePlansRpeTbl.add($rpeActivePlanTbl).find('a.jsExpandCollapse').trigger('click');					
				}
				
			}else if((newUser && rpeYes) || (newUser && rpeNo)){				
				$participantForm.removeClass('closed');				
				$participantDetails.add($showTitle).addClass('closed');
				$('#EnrollmentLookUp1').find('a').trigger('click');
				$('#ssn2_1').val((ssn01.val() == "" || ssn01.val() == null) ? "123" : ssn01.val());
				$('#ssn2_2').val((ssn02.val() == "" || ssn02.val() == null) ? "12" : ssn02.val());
				$('#ssn2_3').val((ssn03.val() == "" || ssn03.val() == null) ? "1234" : ssn03.val());
				$("#lblSSN").html(((ssn01.val() == "" || ssn01.val() == null) ? "123" : ssn01.val()) + '-' + ((ssn02.val() == "" || ssn02.val() == null) ? "12" : ssn02.val()) + '-' + ((ssn03.val() == "" || ssn03.val() == null) ? "1234" : ssn03.val()));
				commonFunc();
				$pageTitleUD.find('.proto_existing').addClass('closed').siblings('.proto_new').add('.unknowParticipant').removeClass('closed');
				
			}else if(noUser && rpeNo){	
				// display error message
				rpeNoUser();
				$showTitle.removeClass('closed');
				
			}else if(noUser && rpeYes){				
				rpeNoUser();			
				//$accessCode1.val('406081');				
				//$allPanels.addClass('closed');
				//$allPanels.next('.content').addClass('hidden');
				//$('#searchAccess').removeClass('btnOff');
				//$('#lookupTable3').removeClass('closed');
				//$('#leftNav').addClass('hidden');
			}
				
	});	
	
	/*================= Look Up Button Functionality ends here for Existing User, New User and No SSN scenarios  =================*/
	
	//Common Function for both New Use and Existing User
	function commonFunc(){		
		$partcipantHeader.find('a').hasClass('collapsed') && $partcipantHeader.find('a').trigger('click');
		// if (existUser) {
			// $eligibleHeader.find('a').hasClass('collapsed') && $eligibleHeader.find('a').trigger('click');			
			// $activeHeader.find('a').hasClass('collapsed') && $activeHeader.find('a').trigger('click');	
		// }
		if (existUser && rpeNo) {
			$showTitle.removeClass('closed');
			$eligibleHeader.find('a').hasClass('collapsed') && $eligibleHeader.find('a').trigger('click');			
			$activeHeader.find('a').hasClass('collapsed') && $activeHeader.find('a').trigger('click');
			$openEnrollment.find('a').hasClass('collapsed') && $openEnrollment.find('a').trigger('click');
		}
		$("#eplans-sec1, #eplans-secView").removeClass("closed");
		$(".noDataEligTable,#eplans-sec2a").addClass("closed");
		$('#activePlanTbl .row1').removeClass("closed");		
		$('.showActiveTable').show();	
	}
	
	// Common Function only for Existing User
	function commonFumc2ExistUser() {
		$mainAlert.removeClass('visible');	
		$participantDetails.removeClass('closed');
		$participantForm.addClass('closed');
		$('#EnrollmentLookUp1').find('a').trigger('click');
		commonFunc();	
	}
	
	/*======== Error Message displayed when you input invalid SSN/NPIN/TIAA-CREF Number  ==========*/
	
	function invalidInput() {	
		$('span[data-alert="msg4"]').removeClass('closed');
		$mainAlert.addClass('visible');	
	}
	
	/*========= Next button functionality for 'No SSN' Scenario =============*/
	
	$('#lookupNextBtn').on('click', function(e) {
		e.preventDefault();
		if (noUser) {			
			$partcipantHeader.find('a').hasClass('collapsed') && $partcipantHeader.find('a').trigger('click');
			$eligibleHeader.find('a').hasClass('expanded') && $eligibleHeader.find('a').trigger('click');
			$openEnrollment.find('a').hasClass('expanded') && $openEnrollment.find('a').trigger('click');
			$('#ActivePlans1').find('a').hasClass('expanded') && $('#ActivePlans1').find('a').trigger('click');
			$mainAlert.hide();
			if($accessCode1.val() === '101010a'){
				$(".elg-plans, .noDataEligTable").addClass("closed");
				$("#eplans-sec2a").removeClass("closed");
			}
		}
	});
	
	/* ========== Access code functionality for 'No SSN' scenario starts here  ============*/
	
	$accessCode1.on('keyup blur', function() {
		var aCodeObj = $("#searchAccess");
			
		($(this).val().length > 0) ? aCodeObj.removeClass("btnOff") : aCodeObj.add(lookupNextBtn).addClass("btnOff");
		$subAlert1.hide();
	});
	
	$('#searchAccess').on('click', function() {
		var newTimer, newTimer1, newTimer2, thisVal = $accessCode1.val();
		clearTimeout(newTimer1);
		$eligiblePlansTable.find('tbody tr').addClass('closed');		
		if (thisVal.length > 0 && /^[a-zA-Z0-9- ]*$/.test(thisVal)) {
			switch (thisVal) {
				case '101010a':
					$lookupProcess.css('visibility', 'visible');
					clearTimeout(newTimer1);
					newTimer1 = setTimeout(function() {
						$('#lookupTableNoData,#lookupTable,#lookupTable2,#lookupTable3,.cName,.cName2').addClass('closed');
						$('#lookupTable1,.cName1,#lookupClientName').removeClass('closed');
						$('#lookupBtns').find('button').removeClass('btnOff');
						$lookupProcess.css('visibility', 'hidden');
						$('#clientName1').html(clientName1);
						$subAlert1.hide();
					}, 1000);
					$(".elg-plans, .noDataEligTable").addClass("closed");
					$("#eplans-sec1,#eplans-sec2a").removeClass("closed");
					$(".dataRow101010a").removeClass('closed');
					break;
				case '405800':
					$lookupProcess.css('visibility', 'visible');
					clearTimeout(newTimer1);
					newTimer1 = setTimeout(function() {
						$('#lookupTableNoData,#lookupTable,#lookupTable1,#lookupTable3,.cName,.cName1').addClass('closed');
						$('#lookupTable2,.cName2').removeClass('closed');
						$('#lookupBtns').find('button').removeClass('btnOff');
						$lookupProcess.css('visibility', 'hidden');
						$('#clientName2').html(clientName2);
						$subAlert1.hide();
					}, 1000);
					$(".elg-plans, .noDataEligTable").addClass("closed");
					$("#eplans-sec1,#eplans-sec2b").removeClass("closed");
					$(".dataRow405800").removeClass('closed');
					break;
				case '403402':
					$lookupProcess.css('visibility', 'visible');
					clearTimeout(newTimer1);
					newTimer1 = setTimeout(function() {
						$('#lookupTableNoData,#lookupTable1,#lookupTable2,#lookupTable3,.cName1,.cName2').addClass('closed');
						$('#lookupTable,.cName').removeClass('closed');
						$('#lookupBtns').find('button').removeClass('btnOff');
						$lookupProcess.css('visibility', 'hidden');
						$('#clientName').html(clientName);
						$subAlert1.hide();
					}, 1000);
					$(".elg-plans, .noDataEligTable").addClass("closed");
					$("#eplans-sec1").removeClass("closed");
					$(".dataRow403402").removeClass('closed');
					break;
				case '406081':
					$lookupProcess.css('visibility', 'visible');					
					clearTimeout(newTimer1);				
					newTimer1 = setTimeout(function() {	
						if(noUser && rpeYes){
							$lookupNextBtn.addClass('btnOff');
							//$('span[data-alert=msg5]').removeClass('closed').siblings().addClass('closed');
							$subAlert1.show();
							$('.invalidACodeMsg').addClass('closed');
							$('.rpeFormMsg').removeClass('closed');
						}
												
						$lookupProcess.css('visibility', 'hidden');
						$('#lookupTableNoData,#lookupTable,#lookupTable1,#lookupTable2').addClass('closed');
						$('#lookupTable3, #lookupClientName, #clientName').removeClass('closed');						
						$('#clientName').text('THE STATE UNIVERSITY OF NEW YORK | THE STATE UNIVERSITY OF NEW YORK');
					//$(".elg-plans, .noDataEligTable").addClass("closed");
					//$("#eplans-sec1,#eplans-sec2").removeClass("closed");
						
						
					},1000);
					break;
					
				default:
					$("#err-acc-code").html($accessCode1.val());					
					$subAlert1.show();
					$('#lookupBtns').find('input:eq(0)').addClass('btnOff');
					$('#clientName,#clientName1,#clientName2').html('');
					$('#lookupTable,#lookupTable1,#lookupTable3, .rpeFormMsg').addClass('closed');
					$('#lookupTableNoData, .invalidACodeMsg').removeClass('closed');
					break;
			}
		}
		return false;
	});
	
	$('#lookupCancelBtn').on('click', function(e) {
		e.preventDefault();
		$('#lookupForm2').addClass('closed');
		$('#lookupForm1').removeClass('closed');
		if(noUser && rpeNo){	
			$partcipantHeader.find('a').hasClass('expanded') && $partcipantHeader.find('a').trigger('click');
			$eligibleHeader.find('a').hasClass('expanded') && $eligibleHeader.find('a').trigger('click');
			$openEnrollment.find('a').hasClass('expanded') && $openEnrollment.find('a').trigger('click');
			$('#ActivePlans1').find('a').hasClass('expanded') && $('#ActivePlans1').find('a').trigger('click');
		}
		$('#lookupTable').addClass('closed');
		$('#lookupTableNoData').removeClass('closed');
		$accessCode1.val('');
		$.merge($("#lookupNextBtn"), $("#searchAccess")).addClass("btnOff");
		$("#mainAlert").hide();
		// if(noUser && rpeYes){			
				// $('#ParticipantDetails, #EligiblePlans, #ActivePlans, #lookupTableNoData').removeClass('closed');
				// $allPanels.next('.content').removeClass('hidden');				
				// $('#leftNav').removeClass('hidden');
		// }
		
	});
	
	/* ======================= Access code functionality for 'No SSN' scenario ends here. ========================*/
	
	/* ======================= Enrollment Look up Panel functionality for all three scenarios ===================*/
	
	$('.elookup').on('keyup', function() {
		var $this = $(this),
			name = $this.attr('name'),			
			numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
			$mainAlert.removeClass('visible');
		if ($this.val() !== '') {
			$(".elookup").attr("disabled", "disabled");
			$('input[name$=' + name + ']').removeAttr("disabled");
			$this.focus();
		} else {
			$(".elookup").removeAttr("disabled");
		}
		
	});

	$('#resetEnb').on('click', function(e) {
		e.preventDefault();
		$(".elookup").removeAttr("disabled");
		$lookupForm1.trigger('reset');
	});
	
	/*========== Functionality for same address checkbox starts ==========*/
	
	$('#sameaddress').on('click', function() {
		$pd = [{
			"ssn2_1": "123",
			"ssn2_2": "456",
			"ssn2_3": "7890",
			"f_name": "John",
			"l_name": "Smith",
			"email": "abcd@tiaa-cref.org",
			"dayphone1": "088",
			"dayphone2": "623",
			"dayphone3": "9786",
			"dob": "06/25/1954",
			"ma_line1": $("#ma_line1").val(),
			"ma_city": $("#ma_city").val(),
			"ma_zip_1": $("#ma_zip_1").val(),
			"ma_zip_2": $("#ma_zip_2").val(),
			"ma_state": $("#ma_state").val()
		}];

		$.each($pd, function(index, obj) {
			$("#ra_line1").val(obj.ma_line1);
			$("#ra_city").val(obj.ma_city);
			$("#ra_zip_1").val(obj.ma_zip_1);
			$("#ra_zip_2").val(obj.ma_zip_2);
			$("#ra_state option[value='" + obj.ma_state + "']").attr('selected', 'selected');
		});
	});

	$('#sameaddress1').on('click', function() {
		$pd = [{
			"ssn2_1": "123",
			"ssn2_2": "456",
			"ssn2_3": "7890",
			"f_name": "John",
			"l_name": "Smith",
			"email": "abcd@tiaa-cref.org",
			"dayphone1": "088",
			"dayphone2": "623",
			"dayphone3": "9786",
			"dob": "06/25/1954",
			"ma_line1": $("#ma_line1").val(),
			"ma_city": $("#ma_city").val(),
			"ma_zip_1": $("#ma_zip_1").val(),
			"ma_zip_2": $("#ma_zip_2").val(),
			"ma_state": $("#ma_state").val()
		}];

		if ($(this).is(':checked')) {
			if (store.get('users') == 'existUser' || store.get('users') == 'newUser') {
				$('#raP_line1').val('26 THOMAS ST');
				$('#raP_line2').val('');
				$('#raP_city').val('STATEN ISLAND');
				$('#raP_state').val('NY');
				$('#raP_zip_1').val('10306');
			} else {
				$.each($pd, function(index, obj) {
					$("#raP_line1").val(obj.ma_line1);
					$("#raP_city").val(obj.ma_city);
					$("#raP_zip_1").val(obj.ma_zip_1);
					$("#raP_zip_2").val(obj.ma_zip_2);
					$("#raP_state option[value='" + obj.ma_state + "']").attr('selected', 'selected');
				});
			}
		} else {
			$("#raP_line1").val('');
			$("#raP_city").val('');
			$("#raP_zip_1").val('');
			$("#raP_zip_2").val('');
			$("#raP_state").val('none');
		}
	});
	
	/*========== End Functionality for same address checkbox ==========*/
	
	/*========== Functionality for Save Button starts here ==========*/
		var addFlag    = false,
			saveBtnCtr = 0,
			ctSave     = 0;
	$('#saveBtn').on('click', function() {
		var dayphoneno = "(" + $('#dayphone1').val() + ")" + " " + $('#dayphone2').val() + " - " + $('#dayphone3').val(),
			evephoneno = "(" + $('#evephone1').val() + ")" + " " + $('#evephone2').val() + " - " + $('#evephone3').val(),
			ssnno = $('#ssn2_1').val() + "-" + $('#ssn2_2').val() + "-" + $('#ssn2_3').val(),
			dob = $('#dob').val(),
			email = $('#email').val(),
			gender = $('#Gender').val(),
			empid = $('#empid').val(),
			mailAdd = $('#ma_line1').val() + [($('#ma_line2').val() != "") ? '<br/>' + $('#ma_line2').val() : ''],
			mailState = $('#ma_city').val() + ', ' + $('#ma_state').val(),
			mailCountry = $('#MA_Country').val(),
			zipcode = $('#ma_zip_1').val(),
			resAdd = $('#ra_line1').val() + [($('#ra_line2').val() != "") ? '<br/>' + $('#ra_line2').val() : ''],
			resState = $('#ra_city').val() + ', ' + $('#ra_state').val(),
			resCountry = $('#RA_Country').val(),
			reszipcode = $('#ra_zip_1').val();// participant details filled in form
			
		$("#Pdalerts,#Pdalerts li").addClass("hidden").removeClass("visible");
		$('#fastLaneTbl,#fastLaneSec1,.openenrollDate').removeClass('closed');

		$("#noFastLaneSDA,#noFastLaneTIAA,.openenrollDate").removeClass("closed");
		$('#fastLaneTbl,#fastLaneSec1').addClass('closed');

		
		$('#fastLaneTblnoData').addClass('closed');
		$("#defaultform [aria-required=true]").each(function() {
			var pDetailsIp = $("#" + this.id),
				pDetailsIpVal = $.trim(pDetailsIp.val());
			pDetailsErr = $("#" + this.id + "-err");
			if (pDetailsIpVal == 0 || typeof pDetailsIpVal == "undefined" || pDetailsIpVal == "none" || pDetailsIpVal == "" || pDetailsIpVal == "mm/dd/yyyy") {
				if (pDetailsErr.length > 0) {
					pDetailsErr.removeClass('hidden');
					saveBtnCtr = 1;
				}
			} else {
				pDetailsErr.addClass('hidden');
			}
		});

		if (saveBtnCtr == 1) {
			$('#Pdalerts').removeClass('hidden').addClass("visible");
			saveBtnCtr = 0;
		} else if (!addFlag && saveBtnCtr == 0) {
			ctSave++;
			$("#Pdalerts,#Pdalerts li").addClass("hidden").removeClass("visible");
			$("#wrapper").find('#Pdalerts').addClass('hidden').removeClass('visible');
			$("#mAddress,#uspsAddress").html(mailAdd + "<br/>" + mailState + " " + zipcode + "<br/>" + mailCountry);
			if(ctSave == 1){
				$('#addressDiscrepancyAlert').addClass('visible');
			}else{
				$("#saveFormData").dialog('open');
				ctSave = 0;
			}
			$('.saveadd').on('click', gotoEligiblePlans);
		} else if (!saveBtnCtr) {

		};
	});
	
	function gotoEligiblePlans() {
		$("#saveFormData").dialog('close');
		$('#EnrollmentLookUp1').find('a').hasClass('expanded') && $('#EnrollmentLookUp1').find('a').trigger('click');
		$eligibleHeader.find('a').hasClass('collapsed') && $eligibleHeader.find('a').trigger('click');
		$("#openEnrollment a").hasClass('collapsed') && $("#openEnrollment a").trigger('click');			
		$('.proto_new').addClass('closed');
		$('#lblSSN').html(ssnno);
		$('#lblmailing_address').html(mailAdd + "<br/>" + mailState + " " + zipcode + "<br/>" + mailCountry);
		$('#lbldaytime_phone').html(dayphoneno);
		$('#lblevening_phone').html(evephoneno);
		$('#lblDOB').html(dob);
		$('#lblEmail').html(email);
		$('#lblgender').html(gender);
		$('#lblempid').html(empid);
		$('#lblresidential_address').html(resAdd + "<br/>" + resState + " " + reszipcode + "<br/>" + resCountry);
		$('.proto_existing').removeClass('closed');
		$('.dataEligTable').removeClass('closed');
		$('.noDataEligTable').addClass('closed');
		// setting data filled in form.
		saveBtnCtr = 1;
		addFlag = true;
	}
	
	/*========== Functionality for Save Button ends here ==========*/
	
	// ssn keyup functionality for enrollment lookup
	$('.ssn').keyup(function() {
		if ($(this).attr('id') == 'ssn') {
			if ($(this).val().length == $(this).attr("maxlength")) {
				$('#ssn1').focus();
			}
		}
		if ($(this).attr('id') == 'ssn1') {
			if ($(this).val().length == $(this).attr("maxlength")) {
				$('#ssn2').focus();
			}
		}
	});
	
	$('.ssn2').keyup(function() {
		if ($(this).attr('id') == 'ssn2_1') {
			if ($(this).val().length == $(this).attr("maxlength")) {
				$('#ssn2_2').focus();
			}
		}
		if ($(this).attr('id') == 'ssn2_2') {
			if ($(this).val().length == $(this).attr("maxlength")) {
				$('#ssn2_3').focus();
			}
		}
	});
	
	$('.phone').keyup(function() {
		if ($(this).attr('id') == 'dayphone1') {
			if ($(this).val().length == $(this).attr("maxlength")) {
				$('#dayphone2').focus();

			}
		}
		if ($(this).attr('id') == 'dayphone2') {
			if ($(this).val().length == $(this).attr("maxlength")) {
				$('#dayphone3').focus();

			}
		}
	});

	$('.phone1').keyup(function() {
		if ($(this).attr('id') == 'evephone1') {
			if ($(this).val().length == $(this).attr("maxlength")) {
				$('#evephone2').focus();

			}
		}
		if ($(this).attr('id') == 'evephone2') {
			if ($(this).val().length == $(this).attr("maxlength")) {
				$('#evephone3').focus();

			}
		}
	});
	
	/*============== Button Functionality for Eligible Plan and Active Plans starts here ==============*/ 
	 
	$(document).on('click','#accessCodeGo', function() {
		var aCode = $("#accessCode").val();		
		if (aCode.length > 0 && /^[a-zA-Z0-9- ]*$/.test(aCode)) {
			$('#subAlert2').removeClass('visible');
		
			if(existUser && rpeNo){			
				$eligiblePlansTable.find('tbody tr').addClass('closed');
			}else if(existUser && rpeYes){			
				$eligiblePlansRpeTbl.find('tbody tr').addClass('closed');
				$eligiblePlansRpeTbl.find('tbody tr.detail').addClass('hidden');
			}
		
			switch (aCode) {
				case '101010a':
					$(".dataRow101010a").removeClass('closed');
					break;
				case '405800':
					$(".dataRow405800").removeClass('closed');
					break;
				case '403402':
					$(".dataRow403402").removeClass('closed');
					break;
				case '15166' :
					$(".dataRow15166").removeClass('closed hidden');
					break;
				case '15167' :
					$(".dataRow15167").removeClass('closed hidden');
					break;
				case '15168' :
					$(".dataRow15168").removeClass('closed hidden');
					break;
				case '406081' :
					$(".dataRow406081").removeClass('closed hidden');
					break;
 
				default:
					$("#err-acc-code2").html(aCode);
					$(".noDataRow").removeClass('closed');
					$('#subAlert2').addClass('visible');
					break;
			}
		}
	});	
	
	$(document).on('click','.ePlanenrollBtn', function() {
		$("#saveResidentialAdd").dialog('open');
		$eligiblePlansTable.find('.actionBtns').removeClass('actionBtns');
		$(this).closest('td').addClass('actionBtns');
		$(".addrFields").addClass('closed');
		$("#USA").removeClass('closed');
		$("#RA_Country1").on('change', function() {
			var country = $(this).val();
			$(".addrFields").addClass('closed');
			$('#' + country).removeClass('closed');
		});
		$("#res-add-alert,#res-add-alert li").addClass("hidden").removeClass("visible");
	});	
	
	$('#disclosure-verification').on('change', function() {
		if ($(this).is(':checked')) {
			$('#saveAddress').removeClass('btnOff');
		} else {
			$('#saveAddress').addClass('btnOff');
		}
	});
	var ctSaveP  = 0;
	$(document).on('click','#saveAddress', function() {
		var saveBtnCtr  = 0;
		$("#USA [aria-required=true]").each(function() {
			var resAddIp = $("#" + this.id),
				resAddIpVal = $.trim(resAddIp.val());
			resAddErr = $("#" + this.id + "-err");
			if (resAddIpVal == 0 || typeof resAddIpVal == "undefined" || resAddIpVal == "none" || resAddIpVal == "" || resAddIpVal == "mm/dd/yyyy") {
				if (resAddErr.length > 0) {
					resAddErr.removeClass('hidden');
					saveBtnCtr = 1;
				}
			} else {
				resAddErr.addClass('hidden');
			}
		});
		// appending address entered by user in modal to participant details saved
		if (store.get('users') === 'existUser') {
			var addrRcount = $('#RA_Country1').val(),
				addrRline1 = $('#raP_line1').val(),
				addrRline2 = $('#raP_line2').val(),
				addrRcity = $('#raP_city').val(),
				addrRstate = $('#raP_state').val(),
				addrRzip = $('#raP_zip_1').val();
			$('#lblresidential_address,#mAddress,#uspsAddress').html(addrRline1+' '+addrRline2+'<br/>'+addrRcity+', '+addrRstate+' '+addrRzip+'<br/>'+addrRcount);
		} else {

		}
		ctSaveP++;		
		if (saveBtnCtr == 1) {
			$('#res-add-alert').removeClass('hidden').addClass("visible");
			saveBtnCtr = 0;
		} else if (saveBtnCtr == 0) {
			if(ctSaveP == 1){
				$('#addressDiscrepancyAlertP').addClass('visible');
			}else{
				$("#saveResidentialAdd").dialog("close");
				$('#res-add-alert').addClass('hidden').removeClass("visible");
				$('#disclosure-verification').attr('disabled', true);
				$('.actionBtns').find('.complete').removeClass('hidden');
				$('.actionBtns').find('.ePlanenrollBtn').addClass('hidden');
				ctSaveP = 0;
			}
		} else {

		}
		enrollDiscFlg = true;
	});
	/*============  Button Functionality for Eligible Plan and Active Plans ends here ==========*/
	
	// Modify SDA and Update button functionality for Plan Details Page starts here
	
		$(document).on('click','.cmmnBtn', function(){
			$(this).closest('.btnBar').closest('.content').prev('div').find('h3').trigger('click');		
		});
	
	// Modify SDA and Update button functionality for Plan Details Page ends here
	
	
	// code for sorting table ID tbl_accesscode
	if($('#tbl_accesscode').length > 0){
		var tbl_persons__js = new tiaacref.tableSort({
			id: '#tbl_accesscode',
			multiSort: false,
			onBeforeSort: function(e){''},
			onAfterSort: function(e){''},
			caseSensitive: false,
			initialSort: ''
		});
		tbl_persons__js.sort( 'accessCodetbl', 'desc', false );
	}

	$("#successCancel").on('click', function() {
		$("#elgbPlnsAdd11 .complete").addClass("hidden");
	});

	var _beam = urlData.getURLname();

	if(_beam.indexOf('beam') > 1){
	// Loading file for Atom Beam 
		$.getScript('assets/js/script.js', function(data){
			new tiaacref.menu({
	            id: "#mmmenu-plan_history",
	            style: "megamenu",
	            attachTo: "#menu-plan_history"
	        });

			var autoincreaseTooltippreview = new tiaacref.tooltip({
	            icon: 'tipLink',
	            id: '#tcId0',
	            size: 'large',
	            detached: false,
	            position: 'top',
	            attachTo: '#tooltip-ERmandatory_0_0_1427899719480',
	            tooltip: '#tooltiptcId0',
	            style: ''
	        });

	        var autoincreaseTooltippreview = new tiaacref.tooltip({
	            icon: 'tipLink',
	            id: '#tcId1',
	            size: 'large',
	            detached: false,
	            position: 'top',
	            attachTo: '#tooltip-mandatory_0_1_1427899719480',
	            tooltip: '#tooltiptcId1',
	            style: ''
	        });

		});
	};
	
	//view button functionality 	
	$(document).on('click', '#viewDetailBtn', function(){
	   window.location.href='./plan1tele_enroll.html';	
	});
	
	//edit link functionality
	var $completeEdit = $('#completeEdit');
	$(document).on('click','.editLink',function(e){
		e.preventDefault();
		var planNum  = $(this).closest('tr').find('td:eq(0)').text(),
			title    = $(this).closest('tr').find('td:eq(1)').text().split('Simulate');
		$completeEdit.find('.dynaTitle').text(title[0] +'- '+planNum);
		$completeEdit.dialog('open');		
	});
	
	$(function() {
		var tableAccTr = $("#tbl_accesscode"),
			universities = ["NYS VDC", "University Of Michigan | University Of Michigan", "Howard University - Howard Medical", "Howard College", "Howard Middle School", "Ron Howard Middle School | Howard College | University - Howard Medical | University Of Howard", "Howard K-6"];
		$("#searchBox-input").autocomplete({
			source: universities,
			select: function(e, ui) {
				$('#acc-reset').trigger('click');
				if (ui.item.value == 'Howard University - Howard Medical') {
					$("#tbl_accesscode tr.hiderow,#sel-loc").removeClass('closed');
				} else if (ui.item.value == 'NYS VDC') {
					$("#sel-loc").removeClass('closed');
					tableAccTr.find("tr[rel=#nys]").removeClass('closed');
				} else if (ui.item.value == 'University Of Michigan | University Of Michigan') {
					$("#sel-loc").removeClass('closed');
					tableAccTr.find("tr[rel=#mic]").removeClass('closed');
				} else if (ui.item.value == 'University Of Michigan | University Of Michigan') {
					$("#sel-loc").removeClass('closed');
					tableAccTr.find("tr[rel=#micN]").removeClass('closed');
				}
			}
		});
	});
	$("#Select_Location1").on('change', function(e) {
		var selRow = $(this).val(),
			searchIp = $("#searchBox-input").val();
		e.preventDefault();
		switch (selRow) {
			case "All":
				$("#tbl_accesscode tr.hiderow").removeClass('closed');
				break;
			case "loc6":
				$("#tbl_accesscode tr.hiderow").addClass('closed');
				if (searchIp == "NYS VDC") {
					$("#tbl_accesscode tr.accRow1").removeClass('closed');
				}
				break;
			default:
				if (searchIp == "NYS VDC") {
					$("#tbl_accesscode tr.hiderow").addClass('closed');
					$("#tbl_accesscode tr.accRow1").removeClass('closed');
				} else {
					$("#tbl_accesscode tr.hiderow").addClass('closed');
					$("#tbl_accesscode tr.hiderow." + selRow).removeClass('closed');
				}
				break;
		}
	});
	
	$('#acc-reset').on('click', function(e) {
		$('#searchBox-input').val('');
		$('#Select_Location1').val('All');
		$("#tbl_accesscode tr.hiderow").addClass('closed');
		$("#sel-loc").addClass('closed');
	});
	
	$("#acc-close").on('click', function() {
		$("#findAccessCode").dialog('close');
	});
	
	/*============  Delete Saved Enrollments starts here ==========*/
	
	$(document).on('click','.delSavedEnroll', function(e){
		e.preventDefault();
		$('#delSEPopup').dialog('open');
	
	});
	
	var   _confirmDelPopup = $('#confirmDelPopup');
	
	$(document).on('click','.delPlan',function(){
		$(this).closest('tr').addClass('delTR').siblings().removeClass('delTR');
		_confirmDelPopup.dialog('open');
	});
	
	$(document).on('click','#delConfirmOk', function(){	
	 
		$('table').find('.delTR').remove();
		_confirmDelPopup.dialog('close');	
		if ($('#savedPopUpTbl1 tbody tr').length === 0){ 
			$('#savedPopUpTbl1 tbody').append('<tr><td colspan="6" class="txtc">- No Data to Display -</td></tr>');
		}		
	});
	
	
	/*============  Delete Saved Enrollments ends here ==========*/

});